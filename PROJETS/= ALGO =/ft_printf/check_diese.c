/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_diese.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 12:05:41 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/13 12:05:42 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_diese_ret(va_list ap, const char *format)
{
	if (*format == 'x' || *format == 'X')
	{
		if ((long)val_ap(ap) == 0)
			return (0);
		else
			return (2);
	}
	else if (*format == 'o' || *format == 'O')
	{
		if ((long)val_ap(ap) == 0)
			return (0);
		else
			return (1);
	}
	return (0);
}

int		check_x(va_list ap, const char *format)
{
	if ((long)val_ap(ap) == 0)
		return (0);
	else
	{
		ft_putstr("0x");
		return (2);
	}
	return (0);
}

int		check_lx(va_list ap, const char *format)
{
	if ((long)val_ap(ap) == 0)
		return (0);
	else
	{
		ft_putstr("0X");
		return (2);
	}
	return (0);
}

int		check_o(va_list ap, const char *format)
{
	if ((long)val_ap(ap) == 0)
		return (0);
	else
	{
		ft_putstr("0");
		return (1);
	}
	return (0);
}

int		check_diese(va_list ap, const char *f)
{
	if (*f == '.')
	{
		if (ft_strchr(f, 'x') && (long)val_ap(ap) != 0)
			return (check_x(ap, f) + check_precision(ap, f + 1, 0));
		else if (ft_strchr(f, 'x') && (long)val_ap(ap) != 0)
			return (check_lx(ap, f) + check_precision(ap, f + 1, 0));
		return (check_precision(ap, f + 1, 0));
	}
	else if (*f >= '0' && *f <= '9' && val_ap(ap) != 0)
		return (put_space_diese(ap, f, 0));
	else if (*f == 'x')
		return (check_x(ap, f));
	else if (*f == 'X')
		return (check_lx(ap, f));
	else if (*f == 'o' || *f == 'O')
		return (check_o(ap, f));
	else if (*f == 'l' || (*f == 'l' && *f + 1 == 'l'))
		return (check_diese_l(ap, f));
	return (0);
}
