/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 15:22:26 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/08 15:22:29 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <limits.h>

# include "libft/libft.h"

int					check_str(const char *f, va_list ap, int i, int ret);

int					var_printf(const char *format, va_list ap);

unsigned int		ft_putunbr(unsigned int nb);
unsigned long		ft_putulnbr(unsigned long nb);
unsigned long long	ft_putullnbr(unsigned long long nb);
long long int		ft_putullintnbr(long long int nb);

int					ft_printf(const char *format, ...);

int					check_conv(const char *format, int i);
char				cc1(char format);
char				cc2(char format);
char				cc3(char format);
int					check_type(va_list ap, char format, const char *f, int i);
int					check_type2(va_list ap, char f, const char *ft, int i);

int					check_flag(va_list ap, const char *f, int i);
int					check_flag2(va_list ap, const char *format, int i);

long				llen_ap(va_list ap);
int					ilen_ap(va_list ap);
int					len_ap(va_list ap, const char *f, int i);
int					val_ap(va_list ap);
char				*val_ap_char(va_list ap);

char				*ft_itoa_base_printf_maj(unsigned int n, int base);
char				*ft_itoa_base_ptf_maj_ull(unsigned long long n, int base);
char				*ft_itoa_base_printf_min(unsigned int n, int base);
char				*ft_itoa_base_long(unsigned long n, int base);
char				*ft_itoa_base_l(long n, int base);
char				*ft_itoa_base_long_long(unsigned long long n, int base);
char				*ft_itoa_base_ll(long long n, int base);
char				*ft_itoa_base_ulint(unsigned long int n, int base);
char				*ft_itoa_base_llint(long long int n, int base);

int					p_s(int res, va_list ap);
int					p_z(int res, va_list ap);

int					check_spe_case(va_list ap, const char *f, int i, int len);
int					spe_case_diese_z(va_list ap, const char *f, int i, int res);
int					put_spe_case(const char *f, int i);

int					check_diese(va_list ap, const char *format);
int					check_zero(va_list ap, const char *f, int i);
int					put_zero_point(va_list ap, const char *f, int i, int res);
int					check_space(va_list ap, const char *format);
int					put_space(va_list ap, const char *format, int i);
int					check_precision(va_list ap, const char *format, int i);
int					check_moins(va_list ap, const char *f, int i);
int					check_moins3(va_list ap, const char *f, int i, int res);
int					check_plus(va_list ap, const char *f, int i);
int					check_z(va_list ap, char format, char check);
int					check_j(va_list ap, char format, char check);
int					check_l(va_list ap, char format, const char *f);
int					check_h(va_list ap, char format, const char *f);
int					check_hh(va_list ap, char format, const char *f);

int					check_diese_l(va_list ap, const char *format);

int					check_diese_moins(va_list ap, const char *f, int i);
int					check_diese_zero(va_list ap, const char *format, int i);

int					check_zero_moins(va_list ap, const char *f, int i, int res);
int					check_zero_plus(va_list ap, const char *format, int i);
int					check_mpp(const char *f, int i, int res, va_list ap);
int					check_mpp_ret(const char *f, int i, va_list ap);
int					return_res2_moins(const char *f, int i, va_list ap);
int					check1(va_list ap, int res, int res2, int len);
int					check2(va_list ap, int res, int res2, int len);

int					return_res2_plus(const char *f, int i);
int					return_res2_big(const char *f, int i, int res2, va_list ap);

int					check_conv_moins(const char *format, int i);
int					check_moins2(va_list ap, const char *f, int i, int res);

int					put_space_diese(va_list ap, const char *format, int i);
int					put_space_plus(va_list ap, const char *f, int i, int res);
int					check_plus_prec(va_list ap, const char *f, int i);
int					put_space_point(va_list ap, const char *f, int i, int res);
int					check_diese_ret(va_list ap, const char *format);
int					put_space_d_prec(va_list ap, const char *f, int i, int res);

int					ft_print_s(va_list ap);
int					ft_print_ls(va_list ap);
int					ft_print_p(va_list ap);
int					ft_print_d(va_list ap);
int					ft_print_ld(va_list ap);
int					ft_print_o(va_list ap);
int					ft_print_lo(va_list ap);
int					ft_print_u(va_list ap);
int					ft_print_lu(va_list ap);
int					ft_print_x(va_list ap);
int					ft_print_lx(va_list ap);
int					ft_print_c(va_list ap);
int					ft_print_lc(va_list ap, wchar_t value);

int					ft_print_d_z(va_list ap);
int					ft_print_o_z(va_list ap);
int					ft_print_u_z(va_list ap);
int					ft_print_x_z(va_list ap);
int					ft_print_lx_z(va_list ap);

int					ft_print_d_j(va_list ap);
int					ft_print_o_j(va_list ap);
int					ft_print_lo_j(va_list ap);
int					ft_print_u_j(va_list ap);
int					ft_print_x_j(va_list ap);
int					ft_print_lx_j(va_list ap);

int					ft_print_x_l(va_list ap, const char *f);
int					ft_print_lx_l(va_list ap, const char *f);
int					ft_print_d_l(va_list ap);
int					ft_print_ld_l(va_list ap);
int					ft_print_lo_l(va_list ap, const char *f);
int					ft_print_o_l(va_list ap, const char *f);
int					ft_print_u_l(va_list ap);
int					ft_print_lu_l(va_list ap, const char *f);

int					ft_print_x_h(va_list ap);
int					ft_print_lx_h(va_list ap);
int					ft_print_d_h(va_list ap);
int					ft_print_ld_h(va_list ap);
int					ft_print_o_h(va_list ap);
int					ft_print_u_h(va_list ap);

int					ft_print_s_point(va_list ap, int res);
int					ft_print_ls_point(va_list ap, int res);
int					ft_print_c_point(va_list ap, int res);
int					ft_print_p_point(va_list ap, int res, const char *f, int i);

int					ft_print_c_moins(va_list ap, const char *f, int i, int res);
int					ft_print_s_moins(va_list ap, const char *f, int i, int res);
int					ft_print_s_m_prc(va_list ap, const char *f, int i, int res);

int					ft_print_b(va_list ap);

int					ft_len_lc(va_list ap, wchar_t value);
int					ft_len_ls(va_list ap);
int					ft_lenlsp(va_list ap, int res);

int					ft_check_p(va_list ap, const char *f, int i, int res);

int					ft_wildcard(va_list ap, const char *f, int i);

#endif
