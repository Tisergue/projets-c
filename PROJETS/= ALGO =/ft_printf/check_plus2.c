/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_plus2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 15:46:37 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/16 15:46:39 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_zero_plus(va_list ap, const char *format, int i)
{
	int		res;
	char	*s;

	s = NULL;
	res = (format[i] >= '0' && format[i] <= '9') ? ft_atoi(&format[i]) : 0;
	if ((long)val_ap(ap) < 0)
	{
		res -= (long)len_ap(ap, format, i);
		if ((res < len_ap(ap, format, i) || res == 0) && val_ap(ap) >= 0)
			return (ft_putchar('+'));
		while (res-- > 0)
			s = ft_properjoin(s, "0");
		ft_printf("-%s%d", s, ((long)val_ap(ap) * -1));
		return (ft_strlen(s) + (long)len_ap(ap, format, i));
	}
	res -= (long)len_ap(ap, format, i) + 1;
	if ((res < len_ap(ap, format, i) || res == 0) && val_ap(ap) >= 0)
		return (ft_putchar('+'));
	while (res-- > 0)
		s = ft_properjoin(s, "0");
	return (ft_printf("+%s", s));
}
