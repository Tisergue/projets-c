/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wildcard.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 15:10:24 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/03 15:10:25 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_wildcard_sc(va_list ap, const char *f, int i, int res)
{
	char	*s;

	s = NULL;
	if (f[i] == 'c')
	{
		if (res == 0)
			return (ft_printf("%c", (long)val_ap(ap)));
		else if (res < 0)
		{
			res = (res * -1) - (long)len_ap(ap, f, i);
			while (res-- > 0)
				s = ft_properjoin(s, " ");
			return (ft_printf("%c", (long)val_ap(ap)) + ft_printf("%s", s));
		}
	}
	else if (f[i] == 's')
		return (ft_print_s(ap));
	return (0);
}

int		ft_wildcard_prc(va_list ap, const char *f, int i, int res)
{
	i = check_conv(f, i);
	if (f[i] == 's')
		return (ft_print_s_point(ap, res));
	if (res == 0)
		return (ft_printf("%d", (long)val_ap(ap)));
	else if (res < 0)
	{
		res = (res * -1) - (long)len_ap(ap, f, i);
		return (ft_printf("%d", (long)val_ap(ap)));
	}
	else
		res -= (long)len_ap(ap, f, i);
	return (p_z(res, ap) + ft_printf("%d", (long)val_ap(ap)));
}

int		ft_wildcard(va_list ap, const char *f, int i)
{
	int		res;

	res = va_arg(ap, int);
	if (f[i - 1] == '.')
		return (ft_wildcard_prc(ap, f, i, res));
	i = check_conv(f, i);
	if (f[i] == 'c' || f[i] == 's')
		return (ft_wildcard_sc(ap, f, i, res));
	if (res == 0)
		return (ft_printf("%d", (long)val_ap(ap)));
	else if (res < 0)
	{
		res = (res * -1) - (long)len_ap(ap, f, i);
		return (ft_printf("%d", (long)val_ap(ap)) + p_s(res, ap));
	}
	else
		res -= (long)len_ap(ap, f, i);
	return (p_s(res, ap) + ft_printf("%d", (long)val_ap(ap)));
}
