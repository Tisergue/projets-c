/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_ls.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:31:37 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:31:38 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_lenlsp(va_list ap, int res)
{
	va_list		aplw;
	wchar_t		*ls;
	wint_t		i;
	int			ret;

	i = 0;
	va_copy(aplw, ap);
	ls = va_arg(aplw, wchar_t *);
	if (ls == NULL)
		return (ft_strlen(ft_putnstr("(null)", res)));
	ret = 0;
	while (ls[i])
	{
		if (ret < res)
		{
			ret += ft_len_lc(aplw, ls[i]);
			if (ret + ls[i + 1] > res)
				return (ret);
		}
		i++;
	}
	va_end(aplw);
	return (ret);
}

int		ft_len_ls(va_list ap)
{
	wchar_t		*ls;
	va_list		apw;
	int			ret;

	va_copy(apw, ap);
	ls = va_arg(apw, wchar_t *);
	if (!ls)
		return (6);
	ret = 0;
	while (*ls)
	{
		ret += ft_len_lc(ap, *ls);
		ls++;
	}
	va_end(apw);
	return (ret);
}

int		ft_print_ls_point(va_list ap, int res)
{
	wchar_t		*ls;
	wint_t		i;
	int			ret;

	i = 0;
	ls = va_arg(ap, wchar_t *);
	if (ls == NULL)
		return (ft_strlen(ft_putnstr("(null)", res)));
	ret = 0;
	while (ls[i])
	{
		if (ret < res)
		{
			ret += ft_print_lc(ap, ls[i]);
		}
		if (ret + ft_len_lc(ap, ls[i + 1]) > res)
			return (ret);
		i++;
	}
	return (ret);
}

int		ft_print_ls(va_list ap)
{
	wchar_t		*ls;
	int			ret;

	ls = va_arg(ap, wchar_t *);
	if (!ls)
	{
		ft_putstr("(null)");
		return (6);
	}
	ret = 0;
	while (*ls)
	{
		ret += ft_print_lc(ap, *ls);
		ls++;
	}
	return (ret);
}
