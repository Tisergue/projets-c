/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_type.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:05:58 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/09 18:06:00 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_type_more4(va_list ap, char format, const char *f, int i)
{
	if (format == 'O')
	{
		while (f[i--] != '%')
		{
			if (val_ap(ap) == 0 && f[i] == '.' && \
				(f[i + 1] == '0' || f[i + 1] == 'O'))
				return (0);
			else if (f[i] == '-' && f[i - 1] == '#')
				return (0);
		}
		return (ft_print_lo(ap));
	}
	return (check_type2(ap, format, f, i));
}

int			check_type_more3(va_list ap, char format, const char *f, int i)
{
	if (format == 'u')
	{
		while (f[i--] != '%')
			if (((f[i] == '.' && f[i + 1] == 'u') || (f[i] == '.' && \
				f[i + 1] == '0' && f[i + 2] == 'u')) && val_ap(ap) == 0)
				return (0);
		return (ft_print_u(ap));
	}
	else if (format == 'U')
	{
		while (f[i--] != '%')
			if (((f[i] == '.' && f[i + 1] == 'U') || (f[i] == '.' && \
				f[i + 1] == '0' && f[i + 2] == 'U')) && val_ap(ap) == 0)
				return (0);
		return (ft_print_lu(ap));
	}
	else if (format == 'C')
		return (ft_print_lc(ap, 0));
	return (check_type_more4(ap, format, f, i));
}

int			check_type_more2(va_list ap, char format, const char *f, int i)
{
	if ((format == 'd' || format == 'i'))
	{
		while (f[--i] != '%')
		{
			if ((long)val_ap(ap) == 0 && f[i] == '.' && \
				(f[i + 1] == '0' || f[i + 1] == 'd' || f[i + 1] == 'i'))
				return (0);
			else if ((val_ap(ap) < 0 && f[i] == '.') \
				|| ((val_ap(ap)) < 0 && f[i] == '0' && f[i + 1] != '0' \
				&& f[i + 1] != cc2(f[i + 1]) && f[i + 1] != cc3(f[i + 1])))
				return (0);
		}
		return (ft_print_d(ap));
	}
	else if (format == 'D')
		return (ft_print_ld(ap));
	return (check_type_more3(ap, format, f, i));
}

int			check_type_more(va_list ap, char format, const char *f, int i)
{
	if (format == 's')
	{
		while (f[--i] != '%')
			if (f[i] == '.' || f[i] == '+')
				return (0);
		return (ft_print_s(ap));
	}
	else if (format == 'p')
	{
		while (f[i--] != '%')
		{
			if (f[i] == '.')
				return (0);
			if (f[i] == '0' && (long)val_ap(ap) == 0)
				return (0);
		}
		ft_putstr("0x");
		return (ft_print_p(ap));
	}
	else if (format == '%' && f[i - 1] == '%')
		return (ft_putchar('%'));
	return (check_type_more2(ap, format, f, i));
}

int			check_type(va_list ap, char format, const char *f, int i)
{
	int		tmp;

	tmp = i;
	while (f[tmp--])
		if ((f[tmp] == '-' && f[tmp - 1] != '%' && \
			(f[tmp + 1] >= '0' && f[tmp + 1] <= '9')) || f[tmp] == '*')
			return (0);
	if (f[i - 1] == 'l' || f[i - 1] == 'z' || f[i - 1] == 'j' \
		|| f[i - 1] == 'h')
		return (0);
	if (format == 'o')
	{
		while (f[--i])
		{
			if (val_ap(ap) == 0 && f[i] == '.' && \
				(f[i + 1] == '0' || f[i + 1] == 'o'))
				return (0);
			else if (f[i] == '-' && f[i - 1] == '#')
				return (0);
		}
		return (ft_print_o(ap));
	}
	return (check_type_more(ap, format, f, i));
}
