/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 19:34:33 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/14 19:34:34 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_error(const char *format, int i)
{
	if (format[i] == '%' && format[i + 1] == ' ')
		return (-1);
	return (0);
}
