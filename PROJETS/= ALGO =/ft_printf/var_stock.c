/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   var_stock.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 16:32:04 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/24 16:32:06 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			var_printf(const char *format, va_list ap)
{
	int			i;
	int			ret;

	i = 0;
	ret = 0;
	return (check_str(format, ap, i, ret));
}
