/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_lx.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:38:56 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:38:57 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_lx_h(va_list ap)
{
	short int		lx;
	char			*s;

	lx = va_arg(ap, int);
	s = ft_itoa_base_printf_maj((unsigned short int)lx, 16);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_lx_l(va_list ap, const char *f)
{
	long int				lx;
	char					*s;

	lx = va_arg(ap, long int);
	s = ft_itoa_base_ptf_maj_ull(lx, 16);
	ft_putstr(s);
	while (*f--)
	{
		if (*f == '#')
			return (ft_strlen(s) + 2);
	}
	return (ft_strlen(s));
}

int		ft_print_lx_j(va_list ap)
{
	unsigned long long		lx;
	char					*s;

	lx = va_arg(ap, unsigned long long);
	s = ft_itoa_base_ptf_maj_ull(lx, 16);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_lx_z(va_list ap)
{
	long long int	lx;
	char			*s;

	lx = va_arg(ap, long long int);
	s = ft_itoa_base_ptf_maj_ull(lx, 16);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_lx(va_list ap)
{
	long	lx;
	char	*s;

	lx = va_arg(ap, long);
	s = ft_itoa_base_printf_maj(lx, 16);
	ft_putstr(s);
	return (ft_strlen(s));
}
