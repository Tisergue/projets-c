/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_moins.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 11:54:54 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/19 11:54:55 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_moins3(va_list ap, const char *f, int i, int res)
{
	int		t[5];

	t[0] = 0;
	t[1] = len_ap(ap, f, i);
	t[1] = check_spe_case(ap, f, i, t[1]);
	t[2] = i;
	if (f[i] == 'p')
		return (ft_check_p(ap, f, i, res));
	while (f[t[2]--] != '%')
	{
		(f[t[2]] == '#') ? (t[0] = 1) : 0;
		(f[t[2]] == '+' && val_ap(ap) >= 0) ? (t[0] = 1) : 0;
	}
	if (res < t[1] && res > 0)
		return (check_type(ap, f[i], &f[i - 1], i));
	if (f[i] == 'd' && val_ap(ap) < 0 && res > 0)
		return (ft_print_d(ap) + p_s(res - t[1], ap));
	if (res == 0)
		return (0);
	return (check_type(ap, f[i], &f[i - 1], i) + p_s(res - t[1], ap) + t[0]);
}

int		check_moins2(va_list ap, const char *f, int i, int res)
{
	char	*s;
	int		p;

	s = NULL;
	p = ft_strlen(ft_itoa_base((long)val_ap(ap), 16)) + 2;
	if (f[i] == 'S')
	{
		p = check_type(ap, f[i], &f[i - 1], i);
		return (p + p_s(res - p, ap));
	}
	else if (f[i] == 'c')
		return (ft_print_c_moins(ap, f, i, res));
	else if (f[i] == '%')
	{
		while (res-- > 1)
			s = ft_properjoin(s, " ");
		return (ft_printf("%%%s", s));
	}
	return (check_moins3(ap, f, i, res));
}

int		check_moins(va_list ap, const char *f, int i)
{
	int		res;
	int		t[3];

	t[0] = check_conv(f, i);
	t[1] = i;
	t[2] = len_ap(ap, f, i) + 1;
	res = (f[i] >= '0' && f[i] <= '9') ? ft_atoi(&f[i]) : 0;
	i = check_conv(f, i);
	while (f[t[0]] != '%')
	{
		if (f[t[0]] == '.' && f[i = check_conv(f, i)] != 's')
			return (check_zero_moins(ap, f, t[0], res));
		else if (f[t[0]] == '.' && f[i = check_conv(f, i)] == 's')
			return (ft_print_s_m_prc(ap, f, t[0], res));
		if (f[t[0]] == ' ' && val_ap(ap) > 0 && (f[i] == 'd' || f[i] == 'i'))
			return (ft_print_d(ap) + p_s(res - t[2], ap) + 1);
		t[0]--;
	}
	if (res == 0 && (f[i] == 'o' || f[i] == 'U' || f[i] == 'X'))
		return (0);
	if (f[i] == 's')
		return (ft_print_s_moins(ap, f, i, res));
	return (check_moins2(ap, f, i, res));
}
