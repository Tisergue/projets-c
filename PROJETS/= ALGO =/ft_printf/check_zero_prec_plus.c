/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_zero_prec_plus.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 22:40:26 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/03 22:40:28 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		return_res_big(const char *f, int i)
{
	int		res;

	while (f[i] != '%')
	{
		if (f[i] == '-')
			res = (f[i + 1] >= '0' && f[i + 1] <= '9') ? \
			ft_atoi(&f[i + 1]) : 0;
		i--;
	}
	return (res);
}

int		return_res2_big(const char *f, int i, int res2, va_list ap)
{
	int		t[4];

	t[0] = 0;
	t[1] = len_ap(ap, f, i);
	t[1] = check_spe_case(ap, f, i, t[1]);
	i = check_conv(f, i);
	t[2] = return_res_big(f, i);
	if (res2 > t[1])
		res2 -= t[1];
	if (f[i] == 'd' || f[i] == 'i')
	{
		(val_ap(ap) >= 0) ? ft_putchar('+') : 0;
		if (val_ap(ap) < 0)
			return (ft_putchar('-') + p_z(res2 + 1, ap) + \
			ft_printf("%d", val_ap(ap) * -1) + p_s(t[2] - res2 - t[1] - 1, ap));
			t[0] = 1;
	}
	return (p_z(res2, ap) + t[0] + check_type(ap, f[i], &f[i - 1], i) + \
			p_s(t[2] - res2 - t[1] - t[0], ap));
}

int		return_res2_plus(const char *f, int i)
{
	int		res2;

	while (f[i] != '%')
	{
		if (f[i] == '.')
			res2 = (f[i + 1] >= '0' && f[i + 1] <= '9') ? \
			ft_atoi(&f[i + 1]) : 0;
		i--;
	}
	return (res2);
}
