/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_j.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 18:10:37 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/20 18:10:38 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_j(va_list ap, char format, char check)
{
	if (format == 'x')
		return (ft_print_x_j(ap));
	else if (format == 'X')
		return (ft_print_lx_j(ap));
	else if (format == 'd' || format == 'i')
		return (ft_print_d_j(ap));
	else if (format == 'D')
		return (ft_print_d_j(ap));
	else if (format == 'o')
		return (ft_print_o_j(ap));
	else if (format == 'O')
		return (ft_print_lo_j(ap));
	else if (format == 'u')
		return (ft_print_u_j(ap));
	else if (format == 'U')
		return (ft_print_u_j(ap));
	return (0);
}
