/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_space2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 13:07:30 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/02 13:07:31 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		p_s_p_ret(va_list ap, const char *f, int i, int res)
{
	int		res2;

	if (f[i] >= '0' && f[i] <= '9')
		res2 = ft_atoi(&f[i]);
	else if (!(f[i] >= '0' && f[i] <= '9'))
		res2 = 0;
	else if ((long)val_ap(ap) == 0)
		return (p_s(res, ap));
	else
		return (p_s(res - (long)len_ap(ap, f, i), ap));
	return (res2);
}

int		put_space_point3(va_list ap, const char *f, int i, int res)
{
	int		t[4];
	char	*check;

	t[0] = p_s_p_ret(ap, f, i, res);
	t[1] = 0;
	t[2] = check_conv(f, i);
	t[3] = len_ap(ap, f, i);
	t[3] = check_spe_case(ap, f, i, t[3]);
	check = "";
	(f[t[2]] == 'p') ? (t[1] = -1) : 0;
	if (res > t[0])
	{
		if (t[0] < t[3] && val_ap(ap) < 0)
			res -= t[3] - t[0] - 1;
		else if (val_ap_char(ap) == check)
			return (p_s(res, ap) + check_precision(ap, f, i));
		((long)val_ap(ap) < 0) ? (t[1] = -1) : 0;
		(t[0] < t[3] && f[i + 1] != 's' && (long)val_ap(ap) > 0) ?
		(res -= t[3] - t[1]) : (res -= t[0] - t[1]);
		if (res > 0)
			return (p_s(res, ap) + check_precision(ap, f, i));
	}
	if ((f[t[2]] == 'U' || f[t[2]] == 'u') && val_ap(ap) < 0)
		return (0);
	return (check_precision(ap, f, i));
}

int		put_space_point2(va_list ap, const char *f, int i, int res)
{
	int		t[4];

	t[0] = p_s_p_ret(ap, f, i, res);
	t[1] = 0;
	t[2] = check_conv(f, i);
	t[3] = 0;
	if (f[t[2]] == 'S')
	{
		if (val_ap(ap) == 0 || val_ap_char(ap) == NULL)
			return (p_s(res - 5, ap) + ft_strlen(ft_putnstr("(null)", 5)));
		else if (res < t[0] && t[0] > ft_lenlsp(ap, res))
			return (ft_print_ls_point(ap, t[0]));
		else if (res > t[0] && t[0] > ft_lenlsp(ap, res))
			return (p_s(res - ft_lenlsp(ap, res), ap) \
				+ ft_print_ls_point(ap, t[0]));
		else if (t[0] == 0)
			return (p_s(res, ap));
	}
	return (put_space_point3(ap, f, i, res));
}

int		put_space_point(va_list ap, const char *f, int i, int res)
{
	int		t[4];

	t[0] = p_s_p_ret(ap, f, i, res);
	t[1] = 0;
	t[2] = check_conv(f, i);
	t[3] = 0;
	if (f[t[2]] == 's')
	{
		if (val_ap(ap) == 0 || val_ap_char(ap) == NULL)
			return (p_s(res - 5, ap) + ft_strlen(ft_putnstr("(null)", 5)));
		else if (res < t[0] && t[0] > ft_strlen(val_ap_char(ap)))
			return (p_s(res - ft_strlen(val_ap_char(ap)), ap) \
				+ check_precision(ap, f, i));
		else if (res > t[0] && t[0] > ft_strlen(val_ap_char(ap)))
			return (p_s(res - ft_strlen(val_ap_char(ap)), ap) \
				+ check_precision(ap, f, i));
		else if (t[0] == 0)
			return (p_s(res, ap));
	}
	if (f[t[2]] == 'c' || f[t[2]] == 'C')
		return (p_s(res - 1, ap));
	return (put_space_point2(ap, f, i, res));
}
