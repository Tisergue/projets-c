/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_space.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 16:45:23 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/13 16:45:24 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		put_space_diese(va_list ap, const char *format, int i)
{
	int		t[3];
	char	*s;

	s = NULL;
	t[1] = check_conv(format, i);
	t[2] = len_ap(ap, format, i);
	t[2] = check_spe_case(ap, format, i, t[2]);
	while (format[t[1]--] != '%')
		if (format[t[1]] == ' ')
			return (put_space(ap, format, t[1] + 1));
	t[0] = (format[i] >= '0' && format[i] <= '9') ? ft_atoi(&format[i]) : 0;
	while (format[i])
	{
		if (format[i] == '.')
			return (put_space_d_prec(ap, format, i + 1, t[0]));
		i++;
	}
	i = check_conv(format, i);
	if (t[0] > t[2])
	{
		t[0] -= t[2] + check_diese_ret(ap, &format[i]);
		return (p_s(t[0], ap) + check_diese(ap, &format[i]));
	}
	return (check_diese(ap, &format[i]));
}

int		put_space_conv2(va_list ap, const char *f, int i, int res)
{
	char	*c;
	int		len;

	len = len_ap(ap, f, i);
	c = "";
	if (f[i] == 's')
	{
		res = (val_ap_char(ap) == c) ? res : res - ft_strlen(val_ap_char(ap));
		return (p_s(res, ap));
	}
	len = check_spe_case(ap, f, i, len);
	if (res > len)
	{
		if (f[i] == 'c')
			return (p_s(res - 1, ap));
		return (p_s(res - len, ap));
	}
	return (0);
}

int		put_space_conv(va_list ap, const char *format, int i, int res)
{
	char	*s;

	s = NULL;
	if (format[i] == 'p')
		return (p_s(res - 2 - (long)len_ap(ap, format, i), ap));
	if (format[i] == 'S')
		return (p_s(res - ft_len_ls(ap), ap));
	if (format[i] == '%')
	{
		res -= 1;
		while (res-- > 0)
			s = ft_properjoin(s, " ");
		ft_printf("%s%%", s);
		return (ft_strlen(s) + 1);
	}
	return (put_space_conv2(ap, format, i, res));
}

int		put_space(va_list ap, const char *format, int i)
{
	int		res;
	int		tmp;
	char	*s;

	s = NULL;
	res = (format[i] >= '0' && format[i] <= '9') ? ft_atoi(&format[i]) : 0;
	if (res == 0 && (long)val_ap(ap) > 0)
		return (ft_putchar(' '));
	tmp = check_conv(format, i);
	while (format[tmp--] != '%')
	{
		if (format[tmp] == '+')
			return (put_space_plus(ap, format, tmp, res));
		if (format[tmp] == '.')
			return (put_space_point(ap, format, tmp + 1, res));
	}
	i = check_conv(format, i);
	return (put_space_conv(ap, format, i, res));
}

int		check_space(va_list ap, const char *format)
{
	int		res;
	int		len;
	char	*s;

	s = NULL;
	len = len_ap(ap, format, *format);
	len = check_spe_case(ap, format, *format, len);
	res = (*format >= '0' && *format <= '9') ? ft_atoi(format) : 0;
	res -= len;
	return (p_s(res, ap));
}
