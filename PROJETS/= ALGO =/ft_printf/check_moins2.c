/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_moins2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 19:32:15 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/11 19:32:16 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_diese_moins(va_list ap, const char *f, int i)
{
	int		t[4];

	t[2] = 0;
	t[1] = i;
	t[3] = i;
	while (f[t[3]++])
		if (f[t[3]] == ' ' && f[t[3] + 1] >= '0' && f[t[3] + 1] <= '9')
		{
			t[0] = ft_atoi(&f[t[3]]);
			return (check_moins3(ap, f, t[3] = check_conv(f, t[3]), t[0]));
		}
	(f[i] >= '0' && f[i] <= '9') ? (t[0] = ft_atoi(&f[i])) : 0;
	while (f[i++])
		if (f[i] == 'x' || f[i] == 'X')
		{
			(f[i] == 'x') ? (ft_putstr("0x")) : (ft_putstr("0X"));
			t[0] -= 2;
			t[2] = 1;
		}
		else if (f[i] == 'o' || f[i] == 'O')
		{
			ft_putchar('0');
			t[0] -= 1;
		}
	return (check_moins2(ap, f, t[1] = check_conv(f, t[1]), t[0]) + t[2]);
}

int		check_conv_moins(const char *format, int i)
{
	char	*convert;
	int		j;

	j = 0;
	convert = "0123456789sSpdDioOuUxXcC%b";
	while (format[i])
	{
		while (convert[j])
		{
			if (convert[j++] == format[i])
				return (i);
		}
		i++;
		j = 0;
	}
	return (0);
}
