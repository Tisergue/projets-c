/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_h.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 21:30:45 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/10 21:30:46 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_hh(va_list ap, char format, const char *f)
{
	signed char		var;
	char			*s;

	if (format == 'C')
		return (ft_print_lc(ap, 0));
	else if (format == 'S')
		return (ft_print_ls(ap));
	var = va_arg(ap, int);
	if (format == 'x')
		s = ft_itoa_base_printf_min((unsigned char)var, 16);
	else if (format == 'X')
		s = ft_itoa_base_printf_maj((unsigned char)var, 16);
	else if (format == 'd' || format == 'i')
		s = ft_itoa(var);
	else if (format == 'o')
		s = ft_itoa_base((unsigned char)var, 8);
	else if (format == 'O')
		s = ft_itoa_base((unsigned short int)var, 8);
	else if (format == 'u')
		s = ft_itoa((unsigned char)var);
	else if (format == 'U' || format == 'D')
		s = ft_itoa((unsigned short int)var);
	ft_putstr(s);
	return (ft_strlen(s));
}

int			check_h(va_list ap, char format, const char *f)
{
	if (format == 'x')
		return (ft_print_x_h(ap));
	else if (format == 'X')
		return (ft_print_lx_h(ap));
	else if (format == 'd' || format == 'i')
		return (ft_print_d_h(ap));
	else if (format == 'D')
		return (ft_print_ld_h(ap));
	else if (format == 'o' || format == 'O')
		return (ft_print_o_h(ap));
	else if (format == 'u')
		return (ft_print_u_h(ap));
	else if (format == 'U')
		return (ft_print_lu(ap));
	return (0);
}
