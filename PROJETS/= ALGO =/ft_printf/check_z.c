/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_z.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 16:26:36 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/20 16:26:37 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_z(va_list ap, char format, char check)
{
	if (format == 'x')
		return (ft_print_x_z(ap));
	else if (format == 'X')
		return (ft_print_lx_z(ap));
	else if (format == 'd' || format == 'i')
		return (ft_print_d_z(ap));
	else if (format == 'o' || format == 'O')
		return (ft_print_o_z(ap));
	else if (format == 'u' || format == 'U' || format == 'D')
		return (ft_print_u_z(ap));
	return (0);
}
