/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ap_cpy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 04:08:14 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/26 04:08:15 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int					ilen_ap(va_list ap)
{
	va_list	ap5;
	char	*ret;

	va_copy(ap5, ap);
	ret = ft_itoa_base_l(va_arg(ap5, int), 10);
	va_end(ap5);
	return (ft_strlen(ret));
}

long				llen_ap(va_list ap)
{
	va_list	ap4;
	char	*ret;

	va_copy(ap4, ap);
	ret = ft_itoa_base_l(va_arg(ap4, long), 10);
	va_end(ap4);
	return (ft_strlen(ret));
}

int					val_ap(va_list ap)
{
	va_list				ap3;
	long				nb;

	va_copy(ap3, ap);
	nb = (va_arg(ap3, long));
	va_end(ap3);
	return (nb);
}

char				*val_ap_char(va_list ap)
{
	va_list	ap2;
	char	*ret;

	va_copy(ap2, ap);
	ret = va_arg(ap2, char *);
	va_end(ap2);
	return (ret);
}

int					len_ap(va_list ap, const char *f, int i)
{
	va_list	ap1;
	char	*ret;

	va_copy(ap1, ap);
	i = check_conv(f, i);
	if (f[i] == 'u' || f[i] == 'U')
		return (llen_ap(ap1));
	else
		return (ilen_ap(ap1));
	va_end(ap1);
	return (0);
}
