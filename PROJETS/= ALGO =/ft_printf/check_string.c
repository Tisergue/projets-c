/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_string.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 16:10:52 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/29 16:10:53 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		cc1(char format)
{
	char	*flag;
	int		i;

	flag = " #0-+.zjlh123456789*";
	i = 0;
	while (flag[i])
	{
		if (flag[i] == format)
			return (format);
		i++;
	}
	return (0);
}

char		cc2(char format)
{
	char	*c;
	int		i;

	c = "sSpdDioOuUxXcCb";
	i = 0;
	while (c[i])
	{
		if (c[i] == format)
			return (format);
		i++;
	}
	return (0);
}

char		cc3(char format)
{
	char	*flag;
	int		i;

	flag = " #-.*";
	i = 0;
	while (flag[i])
	{
		if (flag[i] == format)
			return (format);
		i++;
	}
	return (0);
}

int			check_conv(const char *format, int i)
{
	char	*convert;
	int		j;

	j = 0;
	convert = "sSpdDioOuUxXcC%b";
	if (!format[i + 1])
		return (i);
	while (format[i])
	{
		while (convert[j])
			if (convert[j++] == format[i])
				return (i);
		i++;
		j = 0;
	}
	return (i - 1);
}
