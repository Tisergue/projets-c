/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_printf_maj.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 15:14:27 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/09 15:14:28 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_char_nb(unsigned int n, int base)
{
	int		i;

	i = 1;
	if (n == 0)
		return (1);
	else
	{
		while (n > 0)
		{
			n /= base;
			i++;
		}
	}
	return (i);
}

char		*ft_itoa_base_printf_maj(unsigned int n, int base)
{
	char			*str;
	int				i;

	i = 0;
	str = ft_strnew(ft_char_nb(n, base));
	if (!str || (base < 2 || base > 36))
		return (NULL);
	if (n == 0)
		str[i++] = n + '0';
	while (n > 0)
	{
		if (base > 10 && n % base > 9)
			str[i++] = n % base - 10 + 'A';
		else
			str[i++] = n % base + '0';
		n /= base;
	}
	str[i] = '\0';
	ft_strrev(str);
	return (str);
}
