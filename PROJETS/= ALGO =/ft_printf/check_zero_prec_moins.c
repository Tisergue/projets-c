/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_zero_prec_moins.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 22:02:08 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/29 22:02:09 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_mpp_ret(const char *f, int i, va_list ap)
{
	int		tmp;

	tmp = check_conv(f, i);
	while (f[i] != '%')
	{
		if (f[i] == '+' && val_ap(ap) >= 0 && (f[tmp] == 'd' || f[tmp] == 'i'))
			return (1);
		i--;
	}
	return (0);
}

int		check_mpp(const char *f, int i, int res, va_list ap)
{
	int		tmp;
	int		tmp2;

	tmp2 = check_conv(f, i);
	while (f[i] != '%')
	{
		if (f[i] == '+')
			tmp = i;
		i--;
	}
	if (f[tmp] == '+')
	{
		tmp++;
		res = (f[tmp] >= '0' && f[tmp] <= '9') ? ft_atoi(&f[tmp]) : 0;
		if (val_ap(ap) >= 0 && (f[tmp2] == 'd' || f[tmp2] == 'i'))
			res -= 1;
		if (res < len_ap(ap, f, tmp))
			return (len_ap(ap, f, tmp));
	}
	return (res);
}

int		return_res2_moins(const char *f, int i, va_list ap)
{
	int		res2;

	res2 = 0;
	i = check_conv(f, i);
	while (f[i] != '%')
	{
		if (f[i] == '.')
			res2 = (f[i + 1] >= '0' && f[i + 1] <= '9') ? \
		ft_atoi(&f[i + 1]) : 0;
		i--;
	}
	return (res2);
}

int		check2(va_list ap, int res, int res2, int len)
{
	int		ret;

	ret = (val_ap(ap) * -1);
	if (res == 0 && res2 == 0)
		return (val_ap(ap));
	if (res < res2)
	{
		if (val_ap(ap) < 0)
		{
			ft_putchar('-');
			return (p_z(res2 - len + 1, ap) + ft_printf("%d", ret) + 1);
		}
		else if (res2 < len)
			res2 = 0;
		return (p_z(res2 - len, ap));
	}
	return (0);
}

int		check1(va_list ap, int res, int res2, int len)
{
	if (res < res2)
	{
		if (res2 < len)
			res2 = 0;
		return (p_z(res2 - len, ap));
	}
	return (0);
}
