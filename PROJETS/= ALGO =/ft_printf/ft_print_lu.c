/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_lu.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:32:10 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:32:11 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_lu_l(va_list ap, const char *f)
{
	signed char				lu;
	char					*s;

	lu = va_arg(ap, int);
	s = ft_itoa_base((unsigned short int)lu, 10);
	ft_putstr(s);
	while (*f--)
	{
		if (*f == '#')
			return (ft_strlen(s) + 1);
	}
	return (ft_strlen(s));
}

int		ft_print_lu(va_list ap)
{
	long long int	lu;
	char			*s;

	lu = va_arg(ap, long long int);
	s = ft_itoa_base_long_long(lu, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}
