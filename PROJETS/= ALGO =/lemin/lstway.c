/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstway.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 17:05:51 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/19 17:05:53 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_path	*addpath(t_path *lst, t_path *new)
{
	t_path	*tmp;

	tmp = lst;
	if (!lst)
		return (new);
	while (tmp->n)
		tmp = tmp->n;
	tmp->n = new;
	return (lst);
}

t_path	*newpath(char *value)
{
	t_path	*new;

	new = malloc(sizeof(t_path));
	new->n = NULL;
	new->path = ft_strdup(value);
	return (new);
}

t_way	*addway(t_way *lst, t_way *new)
{
	t_way	*tmp;

	tmp = lst;
	if (!lst)
		return (new);
	while (tmp->n)
		tmp = tmp->n;
	tmp->n = new;
	new->p = tmp;
	return (lst);
}

t_way	*newway(char *value)
{
	t_way	*new;

	new = malloc(sizeof(t_way));
	new->n = NULL;
	new->p = NULL;
	new->way = ft_strdup(value);
	return (new);
}

t_way	*dellst(t_way *way, t_tube *pipe, t_room *room)
{
	t_way	*tmp;
	t_tube	*tmp2;

	tmp = way;
	while (tmp->n)
	{
		tmp2 = pipe;
		while (tmp2)
		{
			if (ft_strcmp(tmp->way, tmp2->p1) == 0)
				if (tmp2->p2 && tmp2->ocup != 1)
					room->check = tmp2->p1;
			if (ft_strcmp(tmp->way, tmp2->p2) == 0)
				if (tmp2->p1 && tmp2->ocup != 1)
					room->check = tmp2->p2;
			tmp2 = tmp2->n;
		}
		tmp = tmp->n;
	}
	if (tmp->p)
		tmp->p->n = NULL;
	else
		tmp->way = NULL;
	return (way);
}
