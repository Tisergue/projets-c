/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ants.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:20:40 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:20:47 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_get_ant(t_lst **stock, t_ant *ant)
{
	t_lst	*tmp;
	int		i;

	tmp = *stock;
	i = 0;
	if (!tmp->data[0])
		ft_error(6);
	while (tmp->data[i])
	{
		if (ft_isdigit(tmp->data[i]) == 0)
			ft_error(0);
		i++;
	}
	ant->nb = ft_atoi(tmp->data);
	if (ant->nb == 0)
		ft_error(0);
}
