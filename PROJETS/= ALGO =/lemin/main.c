/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:28:02 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:28:24 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		main(void)
{
	t_ant	ant;
	t_lst	*stock;
	t_room	*room;
	t_tube	*pipe;

	stock = NULL;
	room = NULL;
	pipe = NULL;
	ft_get_param(&stock, &room);
	ft_get_ant(&stock, &ant);
	ft_get_room(&stock, &room);
	ft_get_pipe(&stock, &pipe, &room);
	ft_solver(&ant, &pipe, &room);
	return (0);
}
