/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   way.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:29:29 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:29:32 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_path	*stock_starts(t_room *room, t_tube *pipe, t_path **path)
{
	t_tube	*tmp;
	int		i;

	tmp = pipe;
	*path = NULL;
	while (tmp)
	{
		if (ft_strcmp(room->start, tmp->p2) == 0 \
			|| ft_strcmp(room->start, tmp->p1) == 0)
		{
			if (ft_strcmp(room->start, tmp->p2) == 0)
				*path = addpath(*path, newpath(tmp->p1));
			if (ft_strcmp(room->start, tmp->p1) == 0)
				*path = addpath(*path, newpath(tmp->p2));
			i = 1;
		}
		tmp = tmp->n;
	}
	if (i != 1)
		ft_error(4);
	return (*path);
}

t_way	*addstart(t_way *way, t_room **room, t_tube *pipe, t_path *path)
{
	t_tube	*tmp;
	int		i;

	tmp = pipe;
	while (tmp)
	{
		if (ft_strcmp((*room)->start, tmp->p1) == 0 && i != 1)
		{
			(*room)->check = path->path;
			way = addway(way, newway((*room)->check));
			i = 1;
		}
		else if (ft_strcmp((*room)->start, tmp->p2) == 0 && i != 1)
		{
			(*room)->check = path->path;
			way = addway(way, newway((*room)->check));
			i = 1;
		}
		tmp = tmp->n;
	}
	if (i != 1)
		ft_error(4);
	return (way);
}

void	check_eop(t_tube *tmp, t_room *room)
{
	t_tube	*tmp3;

	tmp3 = tmp;
	while (tmp3)
	{
		if (room->check_end != 1)
		{
			if (ft_strcmp(room->end, tmp3->p2) == 0)
				room->check_end = 1;
			if (ft_strcmp(room->end, tmp3->p1) == 0)
				room->check_end = 1;
		}
		if (ft_strcmp(room->check, tmp3->p1) == 0)
			if (ft_strcmp(room->end, tmp3->p2) == 0)
				room->eop = 2;
		if (ft_strcmp(room->check, tmp3->p2) == 0)
			if (ft_strcmp(room->end, tmp3->p1) == 0)
				room->eop = 2;
		tmp3 = tmp3->n;
	}
	if (room->check_end != 1)
		ft_error(4);
}

t_way	*create_path(t_way *way, t_room *room, t_tube *tmp, t_tube *tmp2)
{
	check_eop(tmp, room);
	if (ft_strcmp(room->check, tmp2->p1) == 0 \
		&& ft_strcmp(tmp2->p2, room->start) != 0 && room->eop != 2)
	{
		if (tmp2->ocup != 1)
		{
			way = addway(way, newway(tmp2->p2));
			room->check = tmp2->p2;
		}
		(tmp2->ocup == 1) ? (way = dellst(way, tmp, room)) : 0;
		tmp2->ocup = 1;
	}
	else if (ft_strcmp(room->check, tmp2->p2) == 0 \
		&& ft_strcmp(tmp2->p1, room->start) != 0 && room->eop != 2)
	{
		if (tmp2->ocup != 1)
		{
			way = addway(way, newway(tmp2->p1));
			room->check = tmp2->p1;
		}
		(tmp2->ocup == 1) ? (way = dellst(way, tmp, room)) : 0;
		tmp2->ocup = 1;
	}
	return (way);
}

void	get_path(t_tube *pipe, t_room *room, t_way **way, t_path *path)
{
	t_tube	*tmp;
	t_tube	*tmp2;

	pipe->ocup = 0;
	tmp = pipe;
	*way = addstart(*way, &room, pipe, path);
	while (tmp)
	{
		tmp2 = pipe;
		while (tmp2 && room->check && ft_strcmp(room->end, room->check) != 0)
		{
			*way = create_path(*way, room, tmp, tmp2);
			tmp2 = tmp2->n;
		}
		tmp = tmp->n;
	}
	if (room->eop == 2)
		*way = addway(*way, newway(room->end));
	if (!(*way)->way && path->n)
	{
		(path->n) ? (path = path->n) : 0;
		ft_strdel((char **)way);
		room->check = path->path;
		get_path(pipe, room, way, path);
	}
}
