/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tube.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:26:28 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:26:34 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdio.h>

void	check_name(t_room **room, t_tube **pipe)
{
	t_room	*tmp;
	t_tube	*tmp2;
	int		i;
	int		j;

	tmp2 = *pipe;
	while (tmp2)
	{
		tmp = *room;
		i = 0;
		j = 0;
		while (tmp)
		{
			if (ft_strcmp(tmp2->p2, tmp->name) == 0)
				i = 1;
			if (ft_strcmp(tmp2->p1, tmp->name) == 0)
				j = 1;
			tmp = tmp->n;
		}
		if (i == 0 || j == 0)
			ft_error(7);
		tmp2 = tmp2->n;
	}
}

t_tube	*addpipe(t_tube *lst, t_tube *new)
{
	t_tube	*tmp;

	tmp = lst;
	if (lst)
	{
		while (tmp->n)
			tmp = tmp->n;
		tmp->n = new;
	}
	else
		lst = new;
	return (lst);
}

t_tube	*newpipe(char **value)
{
	t_tube	*new;

	new = malloc(sizeof(t_tube));
	new->n = NULL;
	if (value[0] && value[1] && ft_strcmp(value[0], value[1]) == 0)
		ft_error(1);
	if (value[1] && value[0])
	{
		new->p1 = ft_strdup(value[0]);
		new->p2 = ft_strdup(value[1]);
	}
	return (new);
}

void	ft_get_pipe(t_lst **stock, t_tube **pipe, t_room **room)
{
	t_lst	*tmp;
	t_room	*tmp2;
	char	**tab;
	int		i;

	tmp = *stock;
	tmp2 = *room;
	i = 0;
	while (tmp)
	{
		if (ft_strchr(tmp->data, '-'))
		{
			tab = ft_strsplit(tmp->data, '-');
			*pipe = addpipe(*pipe, newpipe(tab));
			i = 1;
		}
		tmp = tmp->n;
	}
	if (i == 0)
		ft_error(8);
	check_name(room, pipe);
	free(tab);
}
