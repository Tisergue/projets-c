/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:22:16 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:22:19 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_way	*reset(t_way *way)
{
	t_way *tmp;

	tmp = way;
	while (tmp)
	{
		tmp->way = NULL;
		tmp = tmp->n;
	}
	return (way);
}

t_var	*march(t_room *room, t_var *var, t_ant *ant, t_way *tmp)
{
	while (var->fmi < var->cpt && var->cpt <= ant->nb && ant->id <= ant->nb)
	{
		ft_printf("L%d-%s ", var->fmi + ant->id, tmp->way);
		if (ft_strcmp(room->end, tmp->way) == 0)
			var->finish = 1;
		if (tmp->p)
			tmp = tmp->p;
		else
			var->fmi = var->cpt;
		var->fmi++;
		if (var->fmi + ant->id > ant->nb)
			var->fmi = var->cpt;
	}
	return (var);
}

void	init_var(t_var **var)
{
	*var = malloc(sizeof(t_var));
	(*var)->fmi = 0;
	(*var)->cpt = 1;
	(*var)->prev = 0;
}

void	print_path(t_ant *ant, t_way **way, t_room *room, t_var *var)
{
	t_way	*tmp;

	ant->id = 1;
	while (*way)
	{
		tmp = *way;
		if (var->fmi < var->cpt && var->cpt <= ant->nb && ant->id <= ant->nb)
			var = march(room, var, ant, tmp);
		ft_printf("\n");
		if (var->finish == 1)
		{
			var->finish = 0;
			ant->id += 1;
			((*way)->p) ? (*way = (*way)->p) : 0;
		}
		if (var->cpt == ant->nb && var->finish == 1)
		{
			var->ok = 1;
			var->cpt--;
		}
		if (var->ok == 0 && var->cpt + 1 <= ant->nb)
			var->cpt++;
		var->fmi = 0;
		*way = (*way)->n;
	}
}

void	ft_solver(t_ant *ant, t_tube **pipe, t_room **room)
{
	t_way	*way;
	t_var	*var;
	t_path	*path;

	init_var(&var);
	path = stock_starts(*room, *pipe, &path);
	get_path(*pipe, *room, &way, path);
	if (!way->way)
		exit(0);
	count_way(ant, &way);
	print_path(ant, &way, *room, var);
}
