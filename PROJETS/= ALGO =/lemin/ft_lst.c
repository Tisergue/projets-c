/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:25:50 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:25:54 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_lst	*ft_lst_addchar(t_lst *lst, t_lst *new)
{
	t_lst	*tmp;

	tmp = lst;
	if (lst)
	{
		while (tmp->n)
			tmp = tmp->n;
		tmp->n = new;
	}
	else
		lst = new;
	return (lst);
}

t_lst	*ft_lst_newchar(char *value)
{
	t_lst	*new;

	new = malloc(sizeof(t_lst));
	new->n = NULL;
	new->data = value;
	return (new);
}
