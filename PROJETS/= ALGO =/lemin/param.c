/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   param.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:26:13 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:26:16 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

char	*add_start_end(char *data)
{
	int		i;
	char	*cpy;

	i = 0;
	while (data[i] != ' ')
		i++;
	cpy = ft_strsub(data, 0, i);
	return (cpy);
}

void	ft_check_start_end(t_lst *stock, t_room *room)
{
	t_lst	*tmp;

	tmp = stock;
	room->check_start = 0;
	room->check_end = 0;
	while (tmp)
	{
		if (ft_strcmp(tmp->data, "##start") == 0)
		{
			(room->check_start == 1) ? ft_error(4) : 0;
			(ft_strchr(tmp->n->data, ' ')) ? \
			room->start = add_start_end(tmp->n->data) : ft_error(4);
			room->check_start = 1;
		}
		if (ft_strcmp(tmp->data, "##end") == 0)
		{
			(room->check_end == 2) ? ft_error(4) : 0;
			(ft_strchr(tmp->n->data, ' ')) ? \
			room->end = add_start_end(tmp->n->data) : ft_error(4);
			room->check_end = 2;
		}
		tmp = tmp->n;
	}
	if (room->check_start == 0 || room->check_end == 0)
		ft_error(4);
}

void	ft_check_param(t_lst *stock)
{
	t_lst	*tmp;

	tmp = stock;
	while (tmp)
	{
		if (tmp->data[0] == 'L')
			ft_error(2);
		if (ft_strchr(tmp->data, '-') && ft_strchr(tmp->data, ' '))
			ft_error(3);
		tmp = tmp->n;
	}
}

void	ft_get_param(t_lst **stock, t_room **room)
{
	t_room	*tmp;
	char	*line;
	int		i;
	int		j;
	int		ret;

	tmp = *room;
	line = NULL;
	i = 0;
	j = 0;
	while ((ret = get_next_line(0, &line)))
	{
		(ret == -1) ? ft_error(10) : 0;
		*stock = ft_lst_addchar(*stock, ft_lst_newchar(line));
		if (line[0] == '\0')
		{
			i = 1;
			break ;
		}
		j = 1;
	}
	(j == 0) ? ft_error(6) : 0;
	(i != 1) ? ft_print(*stock) : 0;
	ft_check_param(*stock);
}
