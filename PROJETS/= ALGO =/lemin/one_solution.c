/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   one_solution.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 18:47:36 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/23 18:47:37 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	one_pipe(t_ant *ant, t_way **way)
{
	char	*str;
	int		i;

	str = (*way)->way;
	i = 1;
	while (i <= ant->nb)
	{
		ft_printf("L%d-%s\n", i, str);
		i++;
	}
	exit(0);
}

void	count_way(t_ant *ant, t_way **way)
{
	t_way	*tmp;
	int		i;

	tmp = *way;
	i = 0;
	while (tmp)
	{
		i++;
		tmp = tmp->n;
	}
	if (i == 1)
		one_pipe(ant, way);
}
