/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:21:25 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:21:30 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H

# define LEMIN_H

# include "libft/libft.h"
# include "libft/ft_printf/ft_printf.h"

typedef struct		s_lst
{
	char			*data;
	struct s_lst	*n;
}					t_lst;

typedef struct		s_ant
{
	int				id;
	int				nb;
	struct s_ant	*n;
}					t_ant;

typedef struct		s_room
{
	char			*name;
	char			*start;
	char			*check;
	char			*cmp;
	char			*end;
	int				check_start;
	int				check_end;
	int				eop;
	struct s_room	*n;
}					t_room;

typedef struct		s_tube
{
	char			*p1;
	char			*p2;
	int				ocup;
	struct s_tube	*n;
}					t_tube;

typedef	struct		s_way
{
	char			*way;
	struct s_way	*p;
	struct s_way	*n;
}					t_way;

typedef struct		s_var
{
	int				fmi;
	int				cpt;
	int				finish;
	int				prev;
	int				ok;
}					t_var;

typedef struct		s_path
{
	char			*path;
	int				check;
	struct s_path	*n;
}					t_path;

void				ft_print(t_lst *file);

void				ft_error(int error);

void				ft_get_param(t_lst **stock, t_room **room);
void				ft_check_param(t_lst *stock);
void				ft_check_start_end(t_lst *stock, t_room *room);

t_lst				*ft_lst_addchar(t_lst *lst, t_lst *new);
t_lst				*ft_lst_newchar(char *value);

t_way				*addway(t_way *lst, t_way *new);
t_way				*newway(char *value);
t_path				*addpath(t_path *lst, t_path *new);
t_path				*newpath(char *value);
t_way				*dellst(t_way *way, t_tube *pipe, t_room *room);

void				ft_get_ant(t_lst **stock, t_ant *ant);

void				ft_get_room(t_lst **stock, t_room **room);

void				ft_get_pipe(t_lst **stock, t_tube **pipe, t_room **room);

t_path				*stock_starts(t_room *room, t_tube *pipe, t_path **path);
void				get_path(t_tube *pipe, t_room *room, \
								t_way **way, t_path *path);

void				ft_solver(t_ant *ant, t_tube **pipe, t_room **room);

void				count_way(t_ant *ant, t_way **way);

#endif
