/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:21:05 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:21:17 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_error(int error)
{
	if (error == 0)
		ft_printf("ERROR : Pas de fourmis ou mauvais format.\n");
	if (error == 1)
		ft_printf("ERROR : Un tube est lié à lui même.\n");
	if (error == 2)
		ft_printf("ERROR : Un nom de salle commence par un L ou #.\n");
	if (error == 3)
		ft_printf("ERROR : Utilisation de '-' dans le nom d'une salle.\n");
	if (error == 4)
		ft_printf("ERROR : Start ou end inexistant ou redéfinie.\n");
	if (error == 5)
		ft_printf("ERROR : Un tube relie une salle non existante.\n");
	if (error == 6)
		ft_printf("ERROR : Pas de map ou mauvais format.\n");
	if (error == 7)
		ft_printf("ERROR : Salle inexistante dans les tubes.\n");
	if (error == 8)
		ft_printf("ERROR : Pas de tubes.\n");
	if (error == 9)
		ft_printf("ERROR : Pas de salles.\n");
	if (error == 10)
		ft_printf("ERROR : Vous tentez de lire un dossier.\n");
	exit(0);
}
