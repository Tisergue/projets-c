/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 17:28:37 by tisergue          #+#    #+#             */
/*   Updated: 2016/05/03 17:28:41 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_room	*addroom(t_room *lst, t_room *new)
{
	t_room	*tmp;

	tmp = lst;
	if (lst)
	{
		while (tmp->n)
			tmp = tmp->n;
		tmp->n = new;
	}
	else
		lst = new;
	return (lst);
}

t_room	*newroom(char *value)
{
	t_room	*new;

	new = malloc(sizeof(t_room));
	new->n = NULL;
	new->name = ft_strdup(value);
	return (new);
}

void	ft_get_room(t_lst **stock, t_room **room)
{
	t_lst	*tmp;
	int		i;
	int		j;
	char	**salle;

	tmp = *stock;
	i = 0;
	j = 0;
	while (tmp)
	{
		if (ft_strchr(tmp->data, ' ') && tmp->data[0] != '#')
		{
			salle = ft_strsplit(tmp->data, ' ');
			*room = addroom(*room, newroom(salle[0]));
			j = 1;
			free(salle);
		}
		tmp = tmp->n;
	}
	if (j == 0)
		ft_error(9);
	ft_check_start_end(*stock, *room);
}
