/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:40:06 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/09 10:40:08 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_struct	*ft_lastelem(t_struct *l_b)
{
	while (l_b->n)
		l_b = l_b->n;
	return (l_b);
}

t_struct	*ft_create_elem(int nb)
{
	t_struct	*new_elem;

	new_elem = (t_struct *)malloc(sizeof(t_struct));
	new_elem->nb = nb;
	new_elem->n = NULL;
	new_elem->p = NULL;
	return (new_elem);
}

t_struct	*ft_push_front(t_struct *pile, int nb)
{
	t_struct	*new_elem;
	t_struct	*tmp;

	tmp = pile;
	new_elem = ft_create_elem(nb);
	if (!pile)
		return (new_elem);
	tmp->p = new_elem;
	new_elem->n = tmp;
	pile = pile->p;
	return (pile);
}

t_struct	*ft_push_back(t_struct *pile, int nb)
{
	t_struct	*new_elem;
	t_struct	*tmp;

	tmp = pile;
	new_elem = ft_create_elem(nb);
	if (!pile)
		return (new_elem);
	while (tmp->n)
		tmp = tmp->n;
	tmp->n = new_elem;
	new_elem->p = tmp;
	return (pile);
}
