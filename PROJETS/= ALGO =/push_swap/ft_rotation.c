/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotation.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:39:45 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/09 10:39:46 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		ft_rr(t_struct *l_a, t_struct *l_b)
{
	t_struct	*tmp;
	t_struct	*tmp2;

	tmp = ft_lastelem(l_a);
	l_a->p = tmp;
	tmp->p->n = NULL;
	l_a->p = tmp;
	tmp->n = l_a;
	tmp->p = NULL;
	while (l_a->p)
		l_a = l_a->p;
	tmp2 = ft_lastelem(l_b);
	l_b->p = tmp2;
	tmp2->p->n = NULL;
	l_b->p = tmp2;
	tmp2->n = l_b;
	tmp2->p = NULL;
	while (l_b->p)
		l_b = l_b->p;
	ft_putstr("rr ");
}

t_struct	*ft_rra(t_struct *l_a, t_struct *l_b)
{
	t_struct	*tmp;

	tmp = ft_lastelem(l_a);
	l_a->p = tmp;
	tmp->p->n = NULL;
	l_a->p = tmp;
	tmp->n = l_a;
	tmp->p = NULL;
	while (l_a->p)
		l_a = l_a->p;
	if (ft_check_order(l_a) == 1 && l_b == NULL)
		ft_putstr("rra\n");
	else
		ft_putstr("rra ");
	return (l_a);
}

t_struct	*ft_rrb(t_struct *l_b)
{
	t_struct	*tmp;

	tmp = ft_lastelem(l_b);
	l_b->p = tmp;
	tmp->p->n = NULL;
	l_b->p = tmp;
	tmp->n = l_b;
	tmp->p = NULL;
	while (l_b->p)
		l_b = l_b->p;
	ft_putstr("rrb ");
	return (l_b);
}

t_struct	*ft_ra(t_struct *l_a, t_struct *l_b)
{
	t_struct	*tmp;

	tmp = ft_lastelem(l_a);
	tmp->n = l_a;
	l_a->n->p = NULL;
	l_a->p = tmp;
	l_a->n = NULL;
	while (l_a->p)
		l_a = l_a->p;
	if (ft_check_order(l_a) == 1 && l_b == NULL)
		ft_putstr("ra\n");
	else
		ft_putstr("ra ");
	return (l_a);
}

t_struct	*ft_rb(t_struct *l_b)
{
	t_struct	*tmp;

	tmp = ft_lastelem(l_b);
	tmp->n = l_b;
	l_b->n->p = NULL;
	l_b->p = tmp;
	l_b->n = NULL;
	while (l_b->p)
		l_b = l_b->p;
	write(1, "rb ", 3);
	return (l_b);
}
