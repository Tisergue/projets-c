/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:39:07 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/09 10:39:10 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_put_error(void)
{
	ft_putstr("Error\n");
	return (0);
}

int		ft_check_flag(t_flag *flag, char **av)
{
	flag->p = 0;
	flag->v = 0;
	flag->check = 0;
	if (av[1][0] == '-' && av[1][1] == 'p')
	{
		flag->p = 1;
		return (1);
	}
	else if (av[1][0] == '-' && av[1][1] == 'v')
	{
		flag->v = 1;
		return (1);
	}
	return (0);
}

int		ft_check_error(char *av, t_flag *flag, char **str, int j)
{
	int		i;

	i = 0;
	flag->check = 0;
	if (ft_atol(&av[i]) > INT_MAX || ft_atol(&av[i]) < INT_MIN)
		return (ft_put_error());
	if (str[1][0] == '-' && j == 1)
		if ((str[1][1] == 'p' && flag->p == 1) \
			|| (str[1][1] == 'v' && flag->v == 1))
			return (1);
	if ((av[i] == '-' || av[i] == '+') && ft_isdigit(av[i + 1]) == 1)
		i++;
	while (av[i])
	{
		if (ft_isdigit(av[i]) == 0)
			return (ft_put_error());
		i++;
	}
	return (1);
}

int		ft_check_list(t_struct *l_a)
{
	t_struct	*tmp;
	t_struct	*check;

	if (!l_a)
		return (0);
	tmp = l_a;
	check = l_a;
	tmp = tmp->n;
	while (check)
	{
		while (tmp)
		{
			if (tmp->nb == check->nb)
				return (ft_put_error());
			tmp = tmp->n;
		}
		check = check->n;
		if (check)
			tmp = check->n;
	}
	if (ft_check_order(l_a) == 1)
		return (0);
	return (1);
}

int		main(int ac, char **av)
{
	t_struct	*l_a;
	t_flag		flag;
	int			i;

	l_a = NULL;
	i = 0;
	if (ac > i + 1)
	{
		while (av[++i])
		{
			ft_check_flag(&flag, av);
			if (ft_check_error(av[i], &flag, av, i) == 0)
				return (0);
			if (av[i][1] != 'v' && av[i][1] != 'p')
				l_a = ft_push_back(l_a, ft_atoi(av[i]));
		}
		if (ft_check_list(l_a) == 0)
			return (0);
		ft_flag(l_a, 0, &flag, 0);
		ft_tri(l_a, &flag);
	}
	return (0);
}
