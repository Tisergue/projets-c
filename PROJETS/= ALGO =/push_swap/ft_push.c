/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:40:17 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/09 10:40:18 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		ft_pa(t_struct **l_a, t_struct **l_b)
{
	*l_a = ft_push_front(*l_a, (*l_b)->nb);
	if ((*l_b)->n)
	{
		*l_b = (*l_b)->n;
		(*l_b)->p->n = NULL;
		(*l_b)->p = NULL;
	}
	else
		*l_b = NULL;
	if (ft_check_order(*l_a) == 1 && !*l_b)
		ft_putstr("pa\n");
	else
		ft_putstr("pa ");
}

void		ft_pb(t_struct **l_a, t_struct **l_b)
{
	*l_b = ft_push_front(*l_b, (*l_a)->nb);
	if ((*l_a)->n)
	{
		*l_a = (*l_a)->n;
		(*l_a)->p->n = NULL;
		(*l_a)->p = NULL;
	}
	else
		*l_a = NULL;
	ft_putstr("pb ");
}
