/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:40:26 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/09 10:40:27 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_struct	*ft_sa(t_struct *l_a, t_struct *l_b)
{
	l_a->p = l_a->n;
	((l_a->n)->n)->p = (l_a->n)->p;
	l_a->n = (l_a->n)->n;
	(l_a->p)->n = l_a;
	l_a = l_a->p;
	if (ft_check_order(l_a) == 1 && !l_b)
		ft_putstr("sa\n");
	else
		ft_putstr("sa ");
	return (l_a);
}

t_struct	*ft_sb(t_struct *l_b)
{
	l_b->p = l_b->n;
	((l_b->n)->n)->p = (l_b->n)->p;
	l_b->n = (l_b->n)->n;
	(l_b->p)->n = l_b;
	l_b = l_b->p;
	ft_putstr("sb ");
	return (l_b);
}
