/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:39:57 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/09 10:39:58 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libft/libft.h"

# include <limits.h>

typedef struct		s_struct
{
	int				nb;
	struct s_struct	*n;
	struct s_struct	*p;
}					t_struct;

typedef struct		s_flag
{
	int				v;
	int				p;
	int				check;
}					t_flag;

void				ft_tri(t_struct *l_a, t_flag *flag);

void				printlist(t_struct *l_a, t_struct *l_b);
void				print_list_bonus(t_struct *l_a);

int					ft_check_order(t_struct *l_a);

t_struct			*ft_push_back(t_struct *pile, int nb);
t_struct			*ft_push_front(t_struct *pile, int nb);
t_struct			*ft_create_elem(int nb);
t_struct			*ft_lastelem(t_struct *l_b);

void				ft_pa(t_struct **l_a, t_struct **l_b);
void				ft_pb(t_struct **l_a, t_struct **l_b);

t_struct			*ft_sa(t_struct *l_a, t_struct *l_b);
t_struct			*ft_sb(t_struct *l_b);

t_struct			*ft_ra(t_struct *l_a, t_struct *l_b);
t_struct			*ft_rb(t_struct *l_b);
t_struct			*ft_rra(t_struct *l_a, t_struct *l_b);
t_struct			*ft_rrb(t_struct *l_b);
void				ft_rr(t_struct *l_a, t_struct *l_b);

void				ft_flag(t_struct *l_a, t_struct *l_b, t_flag *flag, int c);

#endif
