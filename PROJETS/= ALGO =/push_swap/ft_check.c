/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:39:22 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/09 10:39:24 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		ft_flag(t_struct *l_a, t_struct *l_b, t_flag *flag, int c)
{
	if (flag->p == 1 && c == 0)
		print_list_bonus(l_a);
	if (flag->v == 1)
		printlist(l_a, l_b);
}

int			ft_check_order(t_struct *l_a)
{
	if (!l_a || !l_a->n)
		return (1);
	while (l_a->nb < (l_a->n)->nb)
	{
		l_a = l_a->n;
		if (!l_a->n)
			return (1);
	}
	return (0);
}

void		ft_tri_b(t_struct **l_b)
{
	if ((*l_b)->nb < (ft_lastelem(*l_b))->nb)
		*l_b = ft_rb(*l_b);
	if (((*l_b)->n) && (*l_b)->nb < ((*l_b)->n)->nb)
		*l_b = ft_sb(*l_b);
}

void		ft_tri(t_struct *l_a, t_flag *flag)
{
	t_struct	*l_b;
	int			t[2];

	t[0] = 1;
	l_b = NULL;
	while (t[0])
	{
		if (l_a->nb > (ft_lastelem(l_a))->nb)
			l_a = ft_ra(l_a, l_b);
		if (l_a->nb > (l_a->n)->nb)
			l_a = ft_sa(l_a, l_b);
		if ((t[1] = ft_check_order(l_a)) == 0 && l_a->nb < (l_a->n)->nb)
		{
			ft_pb(&l_a, &l_b);
			ft_tri_b(&l_b);
		}
		if (t[1] == 1 && l_b)
		{
			ft_pa(&l_a, &l_b);
			t[1] = ft_check_order(l_a);
		}
		else if (t[1] == 1 && !l_b)
			t[0] = 0;
		ft_flag(l_a, l_b, flag, t[0]);
	}
}
