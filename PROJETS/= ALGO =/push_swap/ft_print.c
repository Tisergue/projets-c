/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:39:33 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/09 10:39:35 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		print_list_bonus(t_struct *l_a)
{
	while (l_a)
	{
		ft_putnbr(l_a->nb);
		ft_putchar(' ');
		l_a = l_a->n;
	}
	ft_putchar('\n');
}

void		printlist(t_struct *l_a, t_struct *l_b)
{
	ft_putstr("\n===> pile a = ");
	while (l_a)
	{
		ft_putnbr(l_a->nb);
		ft_putchar(' ');
		l_a = l_a->n;
	}
	ft_putstr("\n===> pile b = ");
	while (l_b)
	{
		ft_putnbr(l_b->nb);
		ft_putchar(' ');
		l_b = l_b->n;
	}
	ft_putchar('\n');
}
