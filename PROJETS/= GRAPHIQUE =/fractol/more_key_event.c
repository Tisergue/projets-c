/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   more_key_event.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 15:36:51 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/20 15:36:52 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

void	change_fractal(t_env *e)
{
	int	nb_param;

	nb_param = e->param;
	e->param = (nb_param + 1 > 4) ? 1 : nb_param + 1;
	if (e->param == 1)
	{
		init_env_mandelbrot(e);
		mandelbrot(e);
	}
	else if (e->param == 2)
	{
		init_env_julia(e);
		julia(e);
	}
	else if (e->param == 3)
	{
		init_env_burning(e);
		burning_ship(e);
	}
	else if (e->param == 4)
	{
		init_env_tricorn(e);
		tricorn(e);
	}
}

void	change_iteration(int key, t_env *e)
{
	if (key == 69)
		e->iteration += 10;
	else if (key == 78 && e->iteration >= 10)
		e->iteration -= 10;
}
