/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 10:33:27 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/14 10:33:28 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

int		check_param(char **param)
{
	int check;
	int	i;
	int	j;

	check = 0;
	i = 1;
	while (param[i])
	{
		j = -1;
		while (param[i][++j])
			param[i][j] = ft_tolower(param[i][j]);
		(ft_strcmp(param[i], "mandelbrot") == 0) ? draw_mandelbrot(param) : 0;
		(ft_strcmp(param[i], "julia") == 0) ? draw_julia(param) : 0;
		(ft_strcmp(param[i], "burningship") == 0) ? draw_burning(param) : 0;
		(ft_strcmp(param[i], "tricorn") == 0) ? draw_tricorn(param) : 0;
		check = (ft_strcmp(param[i], "-op") == 0) ? 2 : 1;
		i++;
	}
	check = (check == 2) ? 2 : 1;
	return (check);
}

int		main(int ac, char **av)
{
	int		i;
	int		check;

	i = 1;
	if (ac > 1)
	{
		check = check_param(av);
		(check == 1) ? ft_error(1) : aff_op();
	}
	else
		ft_error(2);
	return (0);
}
