/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_event.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 11:18:56 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/14 11:18:57 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

void	aff_to_win(t_env *e)
{
	print_param(e);
	if (e->param == 1)
		mandelbrot(e);
	if (e->param == 2)
		julia(e);
	if (e->param == 3)
		burning_ship(e);
	if (e->param == 4)
		tricorn(e);
}

void	reset(t_env *e)
{
	if (e->param == 1)
	{
		init_env_mandelbrot(e);
		mandelbrot(e);
	}
	if (e->param == 2)
	{
		init_env_julia(e);
		julia(e);
	}
	if (e->param == 3)
	{
		init_env_burning(e);
		burning_ship(e);
	}
	if (e->param == 4)
	{
		init_env_tricorn(e);
		tricorn(e);
	}
}

void	key_color(int color, t_env *e)
{
	if (color == 1)
		e->r = (e->r + 1 > 255) ? 0 : e->r + 1;
	if (color == 2)
		e->g = (e->g + 1 > 255) ? 0 : e->g + 1;
	if (color == 3)
		e->b = (e->b + 1 > 255) ? 0 : e->b + 1;
}

int		key_arrows(int keycode, t_env *e)
{
	if (keycode == 123)
		e->x1 += 100 / e->zoom * 0.1;
	if (keycode == 124)
		e->x1 -= 100 / e->zoom * 0.1;
	if (keycode == 126)
		e->y1 += 100 / e->zoom * 0.1;
	if (keycode == 125)
		e->y1 -= 100 / e->zoom * 0.1;
	return (0);
}

int		key_event(int keycode, t_env *e)
{
	(keycode == 15) ? key_color(1, e) : 0;
	(keycode == 5) ? key_color(2, e) : 0;
	(keycode == 11) ? key_color(3, e) : 0;
	(keycode == 2) ? reset(e) : 0;
	(keycode == 3) ? change_fractal(e) : 0;
	(keycode == 69) ? change_iteration(keycode, e) : 0;
	(keycode == 78) ? change_iteration(keycode, e) : 0;
	if (keycode == 1 && e->stop == 0)
		e->stop = 1;
	else if (keycode == 1 && e->stop == 1)
		e->stop = 0;
	aff_to_win(e);
	if (keycode == 53)
	{
		free(e);
		exit(0);
	}
	return (key_arrows(keycode, e));
}
