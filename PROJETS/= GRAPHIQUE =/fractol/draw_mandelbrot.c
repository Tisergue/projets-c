/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_mandelbrot.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 11:04:58 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/14 11:04:59 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

void			init_env_mandelbrot(t_env *e)
{
	e->param = 1;
	e->iteration = 100;
	e->zoom = 200;
	e->x1 = -2.1;
	e->x2 = 0.6;
	e->y1 = -1.2;
	e->y2 = 1.2;
	e->r = 252;
	e->g = 123;
	e->b = 3;
}

static void		init_var(t_env *e)
{
	t_var	*v;

	v = malloc(sizeof(t_var));
	v->x1 = e->x1;
	v->x2 = e->x2;
	v->y1 = e->y1;
	v->y2 = e->y2;
	v->zoom = e->zoom;
	v->ite_max = e->iteration;
	v->image_x = HWIN;
	v->image_y = WWIN * 0.9;
	v->x = 0;
	e->var = v;
}

t_var			*mandel_val(t_var *v, t_env *e)
{
	v->c_r = v->x / v->zoom + v->x1;
	v->c_i = v->y / v->zoom + v->y1;
	v->z_r = 0;
	v->z_i = 0;
	v->i = 0;
	return (v);
}

int				mandelbrot(t_env *e)
{
	t_var	*v;

	e->img = mlx_new_image(e->mlx, HWIN, WWIN * 0.9);
	e->data = mlx_get_data_addr(e->img, &e->bpp, &e->size, &e->endian);
	init_var(e);
	v = e->var;
	trace_mandelbrot(e, v);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	mlx_destroy_image(e->mlx, e->img);
	return (0);
}

void			draw_mandelbrot(char **param)
{
	t_env	*e;

	e = malloc(sizeof(t_env));
	init_env_mandelbrot(e);
	check_option(param, e);
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, HWIN, WWIN, "Mandelbrot");
	print_param(e);
	mandelbrot(e);
	mlx_mouse_hook(e->win, mouse_event, e);
	mlx_hook(e->win, 2, 0, key_event, e);
	mlx_loop(e->mlx);
}
