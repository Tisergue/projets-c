/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 10:34:31 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/14 10:34:32 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include "../libft/libft.h"
# include "../libft/ft_printf/ft_printf.h"
# include "mlx.h"

# include <math.h>

# define HWIN 640
# define WWIN 480

typedef struct		s_var
{
	double			x1;
	double			x2;
	double			y1;
	double			y2;
	double			zoom;
	double			ite_max;
	double			c_r;
	double			c_i;
	double			z_r;
	double			z_i;
	double			i;
	double			tmp;
	double			image_x;
	double			image_y;
	int				x;
	int				y;
}					t_var;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	void			*img;
	char			*data;
	int				stop;
	int				param;
	int				bpp;
	int				size;
	int				endian;
	int				iteration;
	int				r;
	int				g;
	int				b;
	double			cr;
	double			ci;
	double			zoom;
	double			x1;
	double			x2;
	double			y1;
	double			y2;
	struct s_var	*var;
}					t_env;

/*
**************************************************
**	      CHECK
**************************************************
*/

void				ft_error(int error);

/*
**************************************************
**	      MLX
**************************************************
*/

void				init_env_tricorn(t_env *e);
void				draw_tricorn(char **param);
int					tricorn(t_env *e);
t_var				*tricorn_val(t_var *v, t_env *e);

void				init_env_burning(t_env *e);
void				draw_burning(char **param);
int					burning_ship(t_env *e);
t_var				*burning_val(t_var *v, t_env *e);

void				init_env_mandelbrot(t_env *e);
void				draw_mandelbrot(char **param);
int					mandelbrot(t_env *e);
t_var				*mandel_val(t_var *v, t_env *e);

void				init_env_julia(t_env *e);
void				draw_julia(char **param);
int					julia(t_env *e);
t_var				*julia_val(t_var *v, t_env *e);

void				trace_mandelbrot(t_env *e, t_var *v);
void				trace_julia(t_env *e, t_var *v);
void				trace_burning_ship(t_env *e, t_var *v);
void				trace_tricorn(t_env *e, t_var *v);

/*
**************************************************
**	      EXPOSE
**************************************************
*/

int					key_event(int keycode, t_env *e);

void				change_fractal(t_env *e);

void				change_iteration(int key, t_env *e);

int					mouse_motion_julia(int x, int y, t_env *e);
int					mouse_event(int button, int x, int y, t_env *e);

/*
**************************************************
**	      PRINT
**************************************************
*/

void				print_param(t_env *e);

/*
**************************************************
**	      OPTION
**************************************************
*/

void				check_option(char **param, t_env *e);
void				aff_op(void);

#endif
