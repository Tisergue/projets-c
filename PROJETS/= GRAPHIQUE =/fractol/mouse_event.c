/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_event.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 11:45:09 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/14 11:45:10 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

int		mouse_motion_julia(int x, int y, t_env *e)
{
	if (e->param == 2)
		if (e->stop == 0)
		{
			e->cr = ((float)x - 500) / 512;
			e->ci = ((float)y - 500) / 512;
			mlx_clear_window(e->mlx, e->win);
			julia(e);
		}
	return (0);
}

void	draw_what(t_env *e)
{
	(e->param == 1) ? mandelbrot(e) : 0;
	(e->param == 2) ? julia(e) : 0;
	(e->param == 3) ? burning_ship(e) : 0;
	(e->param == 4) ? tricorn(e) : 0;
}

int		mouse_event(int button, int x, int y, t_env *e)
{
	float	xt;
	float	yt;

	if (y > 0)
	{
		xt = (x / e->zoom) + e->x1;
		yt = (y / e->zoom) + e->y1;
		if (button == 5)
		{
			e->zoom *= 1.1;
			e->x1 = xt - (x / e->zoom);
			e->y1 = yt - (y / e->zoom);
		}
		if (button == 6)
		{
			e->zoom *= 0.9;
			e->x1 = xt - (x / e->zoom);
			e->y1 = yt - (y / e->zoom);
		}
		draw_what(e);
	}
	return (0);
}
