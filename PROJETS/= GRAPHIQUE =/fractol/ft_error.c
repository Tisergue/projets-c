/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 10:37:46 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/14 10:37:47 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

void		ft_error(int error)
{
	ft_putstr("\033[32;33m");
	if (error == 1)
		ft_putendl("\nFractale inconnue.");
	if (error == 2)
		ft_putendl("Pas de paramètre(s).");
	ft_putendl("Tapez: mandelbrot || julia || burningShip || tricorn\n");
	ft_putstr("\033[32;39m");
	exit(0);
}
