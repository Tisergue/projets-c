/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_julia.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 11:05:05 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/14 11:05:06 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

void			init_env_julia(t_env *e)
{
	e->param = 2;
	e->iteration = 100;
	e->zoom = 150;
	e->x1 = -2.2;
	e->x2 = 1.5;
	e->y1 = -1.4;
	e->y2 = 5.2;
	e->cr = 0.35;
	e->ci = 0.05;
	e->r = 252;
	e->g = 123;
	e->b = 3;
	e->stop = 0;
}

static void		init_var(t_env *e)
{
	t_var	*v;

	v = malloc(sizeof(t_var));
	v->x1 = e->x1;
	v->x2 = e->x2;
	v->y1 = e->y1;
	v->y2 = e->y2;
	v->zoom = e->zoom;
	v->ite_max = e->iteration;
	v->image_x = HWIN;
	v->image_y = WWIN * 0.9;
	v->x = 0;
	e->var = v;
}

t_var			*julia_val(t_var *v, t_env *e)
{
	v->c_r = e->cr;
	v->c_i = e->ci;
	v->z_r = v->x / v->zoom + v->x1;
	v->z_i = v->y / v->zoom + v->y1;
	v->i = 0;
	return (v);
}

int				julia(t_env *e)
{
	t_var	*v;

	e->img = mlx_new_image(e->mlx, HWIN, WWIN * 0.9);
	e->data = mlx_get_data_addr(e->img, &e->bpp, &e->size, &e->endian);
	init_var(e);
	v = e->var;
	print_param(e);
	trace_julia(e, v);
	mlx_hook(e->win, 6, 0, mouse_motion_julia, e);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	mlx_destroy_image(e->mlx, e->img);
	return (0);
}

void			draw_julia(char **param)
{
	t_env	*e;

	e = malloc(sizeof(t_env));
	init_env_julia(e);
	check_option(param, e);
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, HWIN, WWIN, "Julia");
	julia(e);
	mlx_mouse_hook(e->win, mouse_event, e);
	mlx_hook(e->win, 2, 0, key_event, e);
	mlx_loop(e->mlx);
}
