/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 21:00:52 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/17 21:00:53 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

void	button_list(t_env *e)
{
	static int	white = 0xFFFFFF;
	static char *stop = "PRESS 's' to STOP motion";
	static char *active = "PRESS 's' to ACTIVATE motion";
	static char *reset = "PRESS 'd' to RESET fractal";
	static char *swap = "PRESS 'f' to CHANGE fractal";

	mlx_string_put(e->mlx, e->win, HWIN * 0.4, WWIN * 0.93, 0xFF00, "OPTION:");
	mlx_string_put(e->mlx, e->win, HWIN * 0.52, WWIN * 0.89, white, reset);
	mlx_string_put(e->mlx, e->win, HWIN * 0.52, WWIN * 0.926, white, swap);
	if (e->stop == 0 && e->param == 2)
		mlx_string_put(e->mlx, e->win, HWIN * 0.52, WWIN * 0.96, white, stop);
	else if (e->stop == 1 && e->param == 2)
		mlx_string_put(e->mlx, e->win, HWIN * 0.52, WWIN * 0.96, white, active);
}

void	print_param(t_env *e)
{
	mlx_clear_window(e->mlx, e->win);
	mlx_string_put(e->mlx, e->win, 0, WWIN * 0.9, 0xFF0000, "R: ");
	mlx_string_put(e->mlx, e->win, 25, WWIN * 0.9, 0xFFFFFF, ft_itoa(e->r));
	mlx_string_put(e->mlx, e->win, 0, WWIN * 0.93, 0x00FF00, "G: ");
	mlx_string_put(e->mlx, e->win, 25, WWIN * 0.93, 0xFFFFFF, ft_itoa(e->g));
	mlx_string_put(e->mlx, e->win, 0, WWIN * 0.96, 0x0000FF, "B: ");
	mlx_string_put(e->mlx, e->win, 25, WWIN * 0.96, 0xFFFFFF, ft_itoa(e->b));
	button_list(e);
}
