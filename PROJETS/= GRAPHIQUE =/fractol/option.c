/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   option.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/18 16:03:02 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/18 16:03:04 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

void	aff_op(void)
{
	ft_putendl("\n===COMMANDE===================");
	ft_putendl("\n-rgb: |R| , |G| , |B| (Exemple: -rgb:255,0,10).\n");
	ft_putendl("\n===EVENTS=====================");
	ft_putendl("\nPRESS 'UP' 'DOWN' 'RIGHT' 'LEFT' to MOVE fractal.");
	ft_putendl("PRESS 'd' to RESET fractal.");
	ft_putendl("PRESS 'f' to CHANGE fractal.");
	ft_putendl("PRESS '+' to INCREASE iteration.");
	ft_putendl("PRESS '-' to DECREASE iteration.");
	ft_putstr("\n\033[32;36mJulia's events ONLY:\033[32;39m");
	ft_putendl(" PRESS 's' to STOP motion.");
	ft_putendl("                     PRESS 's' to ACTIVATE motion.\n");
}

void	op_rgb(char *param, t_env *e)
{
	int		i;
	char	**tab;

	i = 0;
	tab = NULL;
	while (param[i] != ':')
		i++;
	if (ft_strchr(param, ',') != NULL)
	{
		tab = ft_strsplit(param + i + 1, ',');
		e->r = (tab[0] != NULL) ? ft_atoi(tab[0]) : 0;
		e->g = (tab[1] != NULL) ? ft_atoi(tab[1]) : 0;
		e->b = (tab[2] != NULL) ? ft_atoi(tab[2]) : 0;
	}
	else if (param[i + 1])
	{
		tab = ft_strsplit(param + i + 1, '\0');
		e->r = (tab[0] != NULL) ? ft_atoi(tab[0]) : 0;
		e->g = 0;
		e->b = 0;
	}
	e->r = (e->r > 255) ? 255 : e->r;
	e->g = (e->g > 255) ? 255 : e->g;
	e->b = (e->b > 255) ? 255 : e->b;
	free(tab);
}

void	check_option(char **param, t_env *e)
{
	int		i;

	i = 1;
	if (param)
	{
		while (param[i])
		{
			if (ft_strncmp(param[i], "-rgb:", 5) == 0)
				op_rgb(param[i], e);
			if (ft_strcmp(param[i], "-op") == 0)
				aff_op();
			i++;
		}
	}
}
