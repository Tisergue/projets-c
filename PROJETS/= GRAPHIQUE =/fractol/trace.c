/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/16 12:50:21 by tisergue          #+#    #+#             */
/*   Updated: 2016/07/16 12:50:22 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

static t_env	*stock_data(t_var *v, t_env *e)
{
	if (v->i == v->ite_max)
	{
		e->data[(e->size * v->y) + (4 * v->x) + 2] = e->r;
		e->data[(e->size * v->y) + (4 * v->x) + 1] = e->g;
		e->data[(e->size * v->y) + (4 * v->x) + 0] = e->b;
	}
	else
	{
		e->data[(e->size * v->y) + (4 * v->x) + 0] = 0;
		e->data[(e->size * v->y) + (4 * v->x) + 1] = 0;
		e->data[(e->size * v->y) + (4 * v->x) + 2] = v->i * 200;
	}
	return (e);
}

void			trace_burning_ship(t_env *e, t_var *v)
{
	while (v->x < v->image_x)
	{
		v->y = 0;
		while (v->y < v->image_y)
		{
			v = burning_val(v, e);
			while (v->z_r * v->z_r + v->z_i * v->z_i < 4 && v->i < v->ite_max)
			{
				v->tmp = v->z_r;
				v->z_r = fabs(v->z_r * v->z_r - v->z_i * v->z_i + v->c_r);
				v->z_i = fabs(2 * v->z_i * v->tmp + v->c_i);
				v->i++;
			}
			e = stock_data(v, e);
			v->y++;
		}
		v->x++;
	}
}

void			trace_mandelbrot(t_env *e, t_var *v)
{
	while (v->x < v->image_x)
	{
		v->y = 0;
		while (v->y < v->image_y)
		{
			v = mandel_val(v, e);
			while (v->z_r * v->z_r + v->z_i * v->z_i < 4 && v->i < v->ite_max)
			{
				v->tmp = v->z_r;
				v->z_r = v->z_r * v->z_r - v->z_i * v->z_i + v->c_r;
				v->z_i = 2 * v->z_i * v->tmp + v->c_i;
				v->i++;
			}
			e = stock_data(v, e);
			v->y++;
		}
		v->x++;
	}
}

void			trace_julia(t_env *e, t_var *v)
{
	while (v->x < v->image_x)
	{
		v->y = 0;
		while (v->y < v->image_y)
		{
			v = julia_val(v, e);
			while (v->z_r * v->z_r + v->z_i * v->z_i < 4 && v->i < v->ite_max)
			{
				v->tmp = v->z_r;
				v->z_r = v->z_r * v->z_r - v->z_i * v->z_i + v->c_r;
				v->z_i = 2 * v->z_i * v->tmp + v->c_i;
				v->i++;
			}
			e = stock_data(v, e);
			v->y++;
		}
		v->x++;
	}
}
