/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 11:13:54 by tisergue          #+#    #+#             */
/*   Updated: 2016/06/09 18:32:03 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	init_env(t_env *e)
{
	e->ecart = 40;
	e->iso = 57;
	e->z = 1;
	e->color = 1;
	e->screen_w = WWINP;
	e->screen_h = HWINP;
	e->hud = 0;
}

int		main(int ac, char **av)
{
	t_env	*e;
	t_map	*map;
	int		fd;

	ac = 0;
	fd = open(av[1], O_RDONLY);
	e = malloc(sizeof(t_env));
	init_env(e);
	dispfile(fd, e, &map);
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, WWIN, HWIN, "Fil De Fer");
	mlx_key_hook(e->win, key_hook, e);
	mlx_expose_hook(e->win, expose_hook, e);
	mlx_mouse_hook(e->win, mouse_hook, e);
	mlx_loop(e->mlx);
	close(fd);
	return (0);
}
