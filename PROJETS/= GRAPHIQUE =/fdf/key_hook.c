/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/04 14:42:09 by tisergue          #+#    #+#             */
/*   Updated: 2016/06/04 14:42:12 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		key_hook5(int keycode, t_env *e)
{
	if (keycode == 91)
	{
		mlx_clear_window(e->mlx, e->win);
		hud(e);
		e->color += 68000;
		draw(e);
	}
	if (keycode == 87)
	{
		mlx_clear_window(e->mlx, e->win);
		hud(e);
		e->color -= 68000;
		draw(e);
	}
	return (0);
}

int		key_hook4(int keycode, t_env *e)
{
	if (keycode == 69)
	{
		mlx_clear_window(e->mlx, e->win);
		hud(e);
		e->ecart += 1;
		draw(e);
	}
	if (keycode == 78)
	{
		mlx_clear_window(e->mlx, e->win);
		hud(e);
		if (e->ecart - 1 > 0)
			e->ecart -= 1;
		draw(e);
	}
	return (key_hook5(keycode, e));
}

int		key_hook3(int keycode, t_env *e)
{
	if (keycode == 89)
	{
		mlx_clear_window(e->mlx, e->win);
		e->iso += 1;
		hud(e);
		draw(e);
	}
	if (keycode == 86)
	{
		mlx_clear_window(e->mlx, e->win);
		e->iso -= 1;
		hud(e);
		draw(e);
	}
	return (key_hook4(keycode, e));
}

int		key_hook2(int keycode, t_env *e)
{
	if (keycode == 92)
	{
		mlx_clear_window(e->mlx, e->win);
		hud(e);
		e->z += 1;
		draw(e);
	}
	if (keycode == 88)
	{
		mlx_clear_window(e->mlx, e->win);
		hud(e);
		e->z -= 1;
		draw(e);
	}
	return (key_hook3(keycode, e));
}
