/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_o.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/03 14:19:05 by tisergue          #+#    #+#             */
/*   Updated: 2016/06/03 14:19:06 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_map	*gnl(t_env *e)
{
	t_map	*tmp;

	tmp = e->map;
	while (tmp)
	{
		if (tmp->y_n == 1)
			break ;
		tmp = tmp->n;
	}
	return (tmp);
}

void	init_var_o(t_var *var, t_map *map1, t_map *map2, t_env *e)
{
	var = malloc(sizeof(t_var));
	if (ft_atoi(map1->map) != 0)
		var->z = ft_atoi(map1->map) * e->z;
	else
		var->z = ft_atoi(map1->map);
	if (ft_atoi(map2->map) != 0)
		var->z1 = ft_atoi(map2->map) * e->z;
	else
		var->z1 = ft_atoi(map2->map);
	var->x = (map1->x * cos(e->iso) - map1->y * sin(e->iso)) \
				* e->ecart + e->screen_w;
	var->x1 = (map2->x * cos(e->iso) - map2->y * sin(e->iso)) \
				* e->ecart + e->screen_w;
	var->y = (map1->x * sin(e->iso) + map1->y * cos(e->iso)) \
				* e->ecart - var->z + e->screen_h;
	var->y1 = (map2->x * sin(e->iso) + map2->y * cos(e->iso)) \
				* e->ecart - var->z1 + e->screen_h;
	var->cdy = (var->y1 - var->y) / (var->x1 - var->x);
	var->by = var->y - (var->cdy * var->x);
	if (var->z != 0 && var->z1 != 0)
		var->color = 16711680 - (680 * var->z) - e->color;
	if (var->z == 0 || var->z1 == 0)
		var->color = 255;
	e->var = var;
}

void	pente_ord(t_var *var, t_env *e)
{
	void	*mlx;
	void	*win;
	int		c;
	float	len;
	float	i;

	i = 0;
	mlx = e->mlx;
	win = e->win;
	var->nx = var->x1 - var->x;
	var->ny = var->y1 - var->y;
	len = sqrt(var->nx * var->nx + var->ny * var->ny);
	c = var->color;
	while (i < len)
	{
		mlx_pixel_put(mlx, win, var->x + (i / len) * var->nx, \
			var->y + (i / len) * var->ny, c);
		i++;
	}
}

void	draw_o(t_env *e)
{
	t_map	*tmp;
	t_map	*tmp2;
	t_var	*var;

	tmp = e->map;
	tmp2 = gnl(e);
	while (tmp->n && tmp2)
	{
		init_var_o(var, tmp, tmp2, e);
		var = e->var;
		pente_ord(var, e);
		tmp = tmp->n;
		tmp2 = tmp2->n;
	}
}
