/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dispfile.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 15:43:45 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/03 15:43:46 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_map			*addmap(t_map *lst, t_map *new)
{
	t_map	*tmp;

	tmp = lst;
	if (!lst)
		return (new);
	while (tmp->n)
		tmp = tmp->n;
	tmp->n = new;
	new->p = tmp;
	return (lst);
}

t_map			*newmap(char *value, int x, int y)
{
	t_map		*new;

	new = malloc(sizeof(t_map));
	new->p = NULL;
	new->n = NULL;
	new->map = value;
	new->x = x;
	new->y = y;
	new->y_n = y;
	return (new);
}

void			dispfile(int fd, t_env *e, t_map **stock)
{
	char		**tab;
	char		*line;
	int			ret;
	int			i;
	int			j;

	line = NULL;
	j = 0;
	while ((ret = get_next_line(fd, &line)))
	{
		if (ret == -1)
			exit(0);
		tab = ft_strsplit(line, ' ');
		i = 0;
		while (tab[i])
		{
			*stock = addmap(*stock, newmap(tab[i], i, j));
			i++;
		}
		j++;
	}
	(*stock)->y_max = j;
	e->map = *stock;
	free(tab);
}
