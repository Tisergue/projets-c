/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 11:14:15 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/29 11:14:16 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "libft/libft.h"
# include "libft/ft_printf/ft_printf.h"

# include <mlx.h>
# include <math.h>

# define E		20
# define WWIN	2048
# define HWIN	1048
# define WWINP	(WWIN / 3)
# define HWINP	(HWIN / 6)
# define CB		255
# define CR		0xFF0000
# define CG		150

typedef	struct		s_var
{
	float			cdx;
	float			cdy;
	float			bx;
	float			by;
	float			x;
	float			x1;
	float			y;
	float			y1;
	int				nx;
	int				ny;
	int				z;
	int				z1;
	int				color;
}					t_var;

typedef struct		s_map
{
	char			*map;
	float			x;
	float			y;
	int				y_n;
	int				y_max;
	struct s_map	*p;
	struct s_map	*n;
}					t_map;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	int				ecart;
	int				iso;
	int				hud;
	int				z;
	int				color;
	int				screen_w;
	int				screen_h;
	struct s_map	*map;
	struct s_var	*var;
}					t_env;

void				hud(t_env *e);
int					mouse_hook(int boutton, int x, int y, t_env *e);
int					key_hook(int keycode, t_env *e);

int					key_hook2(int keycode, t_env *e);
int					key_hook3(int keycode, t_env *e);
int					key_hook4(int keycode, t_env *e);
int					key_hook5(int keycode, t_env *e);

void				draw_o(t_env *e);

void				draw(t_env *e);
int					expose_hook(t_env *e);

void				dispfile(int fd, t_env *e, t_map **stock);

#endif
