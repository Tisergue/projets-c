/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expose.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 15:25:29 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/03 15:26:05 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	hud(t_env *e)
{
	char	*str1;
	char	*str2;
	char	*str3;
	char	*str4;
	char	*str5;

	if (e->hud == 1)
	{
		str1 = "| '-' & '+' : dezoom & zoom |";
		str2 = "| '9' & '6' : z+ & z-       |";
		str3 = "| '8' & '5' : color         |";
		str4 = "| '7' & '6' : rot+ & rot-   |";
		str5 = "   PRESS 'c' to exit HUD...";
		mlx_string_put(e->mlx, e->win, 0, 0, 0xFFFFFF, str1);
		mlx_string_put(e->mlx, e->win, 0, 20, 0xFFFFFF, str2);
		mlx_string_put(e->mlx, e->win, 0, 40, 0xFFFFFF, str3);
		mlx_string_put(e->mlx, e->win, 0, 60, 0xFFFFFF, str4);
		mlx_string_put(e->mlx, e->win, 0, 100, 0xFF0000, str5);
	}
}

int		mouse_hook(int boutton, int x, int y, t_env *e)
{
	if (boutton == 1)
	{
		mlx_clear_window(e->mlx, e->win);
		e->screen_w = x;
		e->screen_h = y;
		hud(e);
		draw(e);
	}
	return (0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == 53)
		exit(0);
	if (keycode == 8 && e->hud == 0)
	{
		e->hud = 1;
		hud(e);
	}
	else if (keycode == 8 && e->hud == 1)
	{
		mlx_clear_window(e->mlx, e->win);
		e->hud = 0;
		draw(e);
	}
	return (key_hook2(keycode, e));
}
