/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 15:31:13 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/03 15:31:15 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	init_var(t_var *var, t_map *map, t_env *e)
{
	var = malloc(sizeof(t_var));
	if (ft_atoi(map->map) != 0)
		var->z = ft_atoi(map->map) * e->z;
	else
		var->z = ft_atoi(map->map);
	if (ft_atoi(map->n->map) != 0)
		var->z1 = ft_atoi(map->n->map) * e->z;
	else
		var->z1 = ft_atoi(map->n->map);
	var->x = (map->x * cos(e->iso) - map->y * sin(e->iso)) \
				* e->ecart + e->screen_w;
	var->x1 = (map->n->x * cos(e->iso) - map->n->y * sin(e->iso)) \
				* e->ecart + e->screen_w;
	var->y = (map->x * sin(e->iso) + map->y * cos(e->iso)) \
				* e->ecart - var->z + e->screen_h;
	var->y1 = (map->n->x * sin(e->iso) + map->n->y * cos(e->iso)) \
				* e->ecart - var->z1 + e->screen_h;
	var->cdx = (var->y1 - var->y) / (var->x1 - var->x);
	var->bx = var->y - (var->cdx * var->x);
	if (var->z != 0 && var->z1 != 0)
		var->color = 16711680 - (680 * var->z) - e->color;
	if (var->z == 0 || var->z1 == 0)
		var->color = 255;
	e->var = var;
}

void	pente_pos_abs(t_var *var, t_env *e)
{
	void	*mlx;
	void	*win;
	int		c;
	float	len;
	float	i;

	i = 0;
	mlx = e->mlx;
	win = e->win;
	var->nx = var->x1 - var->x;
	var->ny = var->y1 - var->y;
	len = sqrt(var->nx * var->nx + var->ny * var->ny);
	c = var->color;
	while (i < len)
	{
		mlx_pixel_put(mlx, win, var->x + (i / len) * var->nx, \
			var->y + (i / len) * var->ny, c);
		i++;
	}
}

void	draw(t_env *e)
{
	t_map	*tmp;
	t_var	*var;

	tmp = e->map;
	draw_o(e);
	while (tmp->n)
	{
		if (tmp->y_n == tmp->n->y_n)
		{
			init_var(var, tmp, e);
			var = e->var;
			pente_pos_abs(var, e);
		}
		tmp = tmp->n;
	}
}

int		expose_hook(t_env *e)
{
	draw(e);
	return (0);
}
