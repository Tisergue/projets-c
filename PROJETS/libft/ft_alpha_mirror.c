/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_alpha_mirror.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <tisergue@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 13:03:55 by tisergue          #+#    #+#             */
/*   Updated: 2015/07/29 18:04:23 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_alpha_mirror(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= 'a' && str[i] <= 'z')
		{
			if (str[i] > 'm')
				ft_putchar('m' - (str[i] - 'n'));
			else if (str[i] < 'n')
				ft_putchar('m' - (str[i] - 'n'));
		}
		else if (str[i] >= 'A' && str[i] <= 'Z')
		{
			if (str[i] > 'M')
				ft_putchar('M' - (str[i] - 'N'));
			else if (str[i] < 'N')
				ft_putchar('M' - (str[i] - 'N'));
		}
		else
			ft_putchar(str[i]);
		i++;
	}
	return (0);
}
