/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 18:22:37 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/24 14:33:24 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	int		i;
	int		j;
	char	*cpy;

	if (s == '\0')
		return (NULL);
	i = ft_strlen(s);
	cpy = (char *)malloc(sizeof(char) * (i + 1));
	if (cpy == '\0')
		return (NULL);
	j = 0;
	while (j < i)
	{
		cpy[j] = s[j];
		j++;
	}
	cpy[j] = '\0';
	return (cpy);
}
