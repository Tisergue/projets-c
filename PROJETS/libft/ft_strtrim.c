/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:07:37 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/01 12:08:34 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int		i;
	int		size;
	char	*res;

	if (s == NULL)
		return (NULL);
	i = 0;
	size = ft_strlen(s);
	res = (char *)malloc(sizeof(char) * (size + 1));
	while (s[size - 1] == ' ' || s[size - 1] == '\t' || s[size - 1] == '\n')
		size--;
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
	{
		i++;
		size--;
	}
	s = s + i;
	i = 0;
	while (i < size)
	{
		res[i] = *s++;
		i++;
	}
	res[i] = '\0';
	return (res);
}
