/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 14:20:11 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/05 14:42:17 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*ret;

	ret = (t_list *)malloc(sizeof(t_list));
	ret = f(lst);
	if (lst->next)
		ret->next = ft_lstmap(lst->next, f);
	else
		ret->next = NULL;
	return (ret);
}
