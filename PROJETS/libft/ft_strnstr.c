/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 13:34:51 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/26 13:34:54 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *lil, size_t len)
{
	char	*b;
	char	*l;
	size_t	save;

	if (!*lil)
		return ((char *)big);
	while (*big && len > 0)
	{
		save = len;
		if (*big == *lil)
		{
			b = (char *)big;
			l = (char *)lil;
			while (*b++ == *l++ && save > 0)
			{
				save--;
				if (!*l)
					return ((char *)big);
			}
		}
		big++;
		len--;
	}
	return (NULL);
}
