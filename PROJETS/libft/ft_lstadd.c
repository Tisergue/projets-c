/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 14:19:50 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/04 14:19:51 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd(t_list **alst, t_list *new)
{
	t_list	*list;

	if (new && alst)
	{
		list = (t_list *)malloc(sizeof(t_list));
		new->next = *alst;
		*alst = new;
	}
}
