/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 15:10:48 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/14 15:12:44 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char *s)
{
	char	stock;
	char	*str;

	str = s + ft_strlen(s) - 1;
	while (s < str)
	{
		stock = *s;
		*s++ = *str;
		*str-- = stock;
	}
	return (s);
}
