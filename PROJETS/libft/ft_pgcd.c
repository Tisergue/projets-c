/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pgcd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <tisergue@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/31 16:26:09 by tisergue          #+#    #+#             */
/*   Updated: 2015/07/31 19:04:45 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_pgcd(int nb1, int nb2)
{
	int d;

	while (nb2 > 0)
	{
		d = nb1 % nb2;
		nb1 = nb2;
		nb2 = d;
	}
	return (nb1);
}
