/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_flag.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:42:25 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/09 18:42:27 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_flag_more3(va_list ap, const char *format, int i)
{
	if (format[i] == 'l')
	{
		if (format[i + 1] == 'l')
			return (check_l(ap, format[i + 2], format));
		return (check_l(ap, format[i + 1], format));
	}
	else if (format[i] == 'h')
	{
		if (format[i] == 'h' && format[i + 1] == cc2(format[i + 1]))
			return (check_h(ap, format[i + 1], format));
		else if (format[i + 1] == 'h' && format[i + 2] == cc2(format[i + 2]))
			return (check_hh(ap, format[i + 2], format));
		else
			return (check_flag(ap, format, i + 2));
	}
	return (check_flag2(ap, format, i));
}

int		check_flag_more2(va_list ap, const char *format, int i)
{
	int		tmp;

	if (format[i] == '-')
	{
		i++;
		tmp = i;
		tmp = check_conv(format, tmp);
		if (format[i] == '#')
			return (check_diese_moins(ap, format, i + 1));
		if (format[i] == ' ')
		{
			((format[tmp] == 'd' || format[tmp] == 'i') && val_ap(ap) > 0) \
			? ft_putchar(' ') : 0;
			i++;
		}
		else if (format[i] == '+' && val_ap(ap) >= 0)
		{
			(format[tmp] == 'd' || format[tmp] == 'i') ? ft_putchar('+') : 0;
			i++;
		}
		else if (format[i] == '.')
			return (check_precision(ap, format, i + 1));
		return (check_moins(ap, format, i));
	}
	return (check_flag_more3(ap, format, i));
}

int		check_flag_more(va_list ap, const char *format, int i)
{
	int		tmp;

	if (format[i] == '+')
	{
		tmp = check_conv(format, i);
		while (format[tmp--] != '%')
			if (format[tmp] == '.')
				return (check_plus_prec(ap, format, i + 1));
		return (check_plus(ap, format, i + 1));
	}
	else if (format[i] == '0' || (format[i] == '#' && format[i + 1] == '0'))
	{
		if (format[i] == '#' && format[i + 1] == '0')
			return (check_diese_zero(ap, format, i + 2));
		while (format[i] == '0')
			i++;
		if (format[i] == ' ' && (long)val_ap(ap) > 0)
			return (check_flag(ap, format, i));
		if (format[i] == '-')
			return (check_moins(ap, format, i + 1));
		return (check_zero(ap, format, i));
	}
	return (check_flag_more2(ap, format, i));
}

int		check_flag(va_list ap, const char *f, int i)
{
	if (f[i] == ' ')
	{
		while (f[i++])
		{
			if (f[i] != cc2(f[i]) && f[i] != cc1(f[i]))
				return (ft_putchar(f[i]));
			else if (!(f[i] >= '0' && f[i] <= '9'))
			{
				if ((f[i] == 'd' || f[i] == 'i') && (long)val_ap(ap) >= 0)
					return (ft_putchar(' '));
				return (check_flag(ap, f, i));
			}
			else if (f[i] != '0')
				return (put_space(ap, f, i));
			else
				return (check_zero(ap, f, i));
			return (check_space(ap, &f[i]));
		}
	}
	else if (f[i] == '.')
		return (check_precision(ap, f, i + 1));
	return (check_flag_more(ap, f, i));
}
