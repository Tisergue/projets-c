/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_s.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:23:03 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:23:16 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_s_m_prc(va_list ap, const char *f, int i, int res)
{
	int		res2;
	int		p;
	char	*s;

	i++;
	s = NULL;
	res2 = (f[i] >= '0' && f[i] <= '9') ? ft_atoi(&f[i]) : 0;
	p = ft_print_s_point(ap, res2);
	res -= p;
	while (res-- > 0)
		s = ft_properjoin(s, " ");
	ft_putstr(s);
	return (ft_strlen(s) + p);
}

int		ft_print_s_moins(va_list ap, const char *f, int i, int res)
{
	char	*s;
	int		tmp;

	s = NULL;
	if (f[i] == 's')
	{
		tmp = ft_print_s(ap);
		res -= tmp;
		while (res-- > 0)
			s = ft_properjoin(s, " ");
		ft_putstr(s);
	}
	return (ft_strlen(s) + tmp);
}

int		ft_print_s_point(va_list ap, int res)
{
	char	*s;

	s = va_arg(ap, char *);
	if (s == NULL)
	{
		ft_putnstr("(null)", res);
		return (res);
	}
	if (res < ft_strlen(s))
	{
		ft_putnstr(s, res);
		return (res);
	}
	else
		ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_s(va_list ap)
{
	char	*s;

	s = va_arg(ap, char *);
	if (s == NULL)
	{
		ft_putstr("(null)");
		return (6);
	}
	ft_putstr(s);
	return (ft_strlen(s));
}
