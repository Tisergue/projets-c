/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_c.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:26:56 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:26:57 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_c_moins(va_list ap, const char *f, int i, int res)
{
	char	*s;

	s = NULL;
	if (f[i] == 'c')
	{
		res -= ft_print_c(ap);
		while (res-- > 0)
			s = ft_properjoin(s, " ");
		ft_putstr(s);
	}
	return (ft_strlen(s) + 1);
}

int		ft_print_c_point(va_list ap, int res)
{
	int		c;

	c = va_arg(ap, int);
	res = 0;
	return (ft_putchar(c));
}

int		ft_print_c(va_list ap)
{
	int		c;

	c = va_arg(ap, int);
	if (c == 0)
	{
		write(1, "\0", 1);
		return (1);
	}
	ft_putchar(c);
	return (1);
}
