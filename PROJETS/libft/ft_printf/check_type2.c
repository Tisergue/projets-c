/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_type2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 17:03:48 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/26 17:03:49 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_type2_more(va_list ap, char f, const char *ft, int i)
{
	if (f == 'x')
	{
		while (ft[i--] != '%')
		{
			if ((ft[i] == '#' && ft[i + 1] != 'x' && val_ap(ap) == 0) || \
				(ft[i] == '#' && ft[i - 1] == 'l'))
				return (0);
			if (((ft[i] == '.' && ft[i + 1] == 'x') || (ft[i] == '.' && \
				ft[i + 1] == '0' && ft[i + 2] == 'x')) && val_ap(ap) == 0)
				return (0);
		}
		return (ft_print_x(ap));
	}
	else if (f == 'c')
	{
		while (ft[i--] != '%')
		{
			if (ft[i] == '.')
				return (0);
		}
		return (ft_print_c(ap));
	}
	return (0);
}

int			check_type2(va_list ap, char f, const char *ft, int i)
{
	if (f == 'b')
		return (ft_print_b(ap));
	else if (f == 'S')
	{
		while (ft[i--])
		{
			if (ft[i] == '-')
				return (0);
			if (ft[i] == '.')
				return (0);
		}
		return (ft_print_ls(ap));
	}
	else if (f == 'X')
	{
		while (ft[i--] != '%')
		{
			if ((ft[i] == '#' && ft[i + 1] != 'X' && val_ap(ap) == 0) || \
				(((ft[i] == '.' && ft[i + 1] == 'X') || (ft[i] == '.' && \
				ft[i + 1] == '0' && ft[i + 2] == 'X')) && val_ap(ap) == 0))
				return (0);
		}
		return (ft_print_lx(ap));
	}
	return (check_type2_more(ap, f, ft, i));
}
