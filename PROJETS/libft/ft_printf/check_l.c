/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_l.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 22:42:36 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/07 22:42:37 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_diese_l_after(va_list ap, const char *format)
{
	char	*tmp;

	tmp = (char *)format;
	while (format++)
	{
		if (*format == 'x')
		{
			ft_putstr("0x");
			return (ft_print_x_l(ap, format));
		}
		else if (*format == 'X')
		{
			ft_putstr("0X");
			return (ft_print_lx_l(ap, format));
		}
		else if (*format == 'o' || *format == 'O')
		{
			ft_putchar('0');
			return (ft_print_o_l(ap, format));
		}
		else if (*format == 'u')
			return (ft_print_u_l(ap));
	}
	return (0);
}

int			check_diese_l(va_list ap, const char *format)
{
	char	*tmp;

	tmp = (char *)format;
	while (format++)
	{
		if (*format == 'x')
		{
			ft_putstr("0x");
			return (check_flag(ap, tmp, 0));
		}
		else if (*format == 'X')
		{
			ft_putstr("0X");
			return (check_flag(ap, tmp, 0));
		}
		else if (*format == 'o' || *format == 'O')
		{
			ft_putchar('0');
			return (check_flag(ap, tmp, 0));
		}
	}
	return (0);
}

int			check_l(va_list ap, char format, const char *f)
{
	if (format == 'x')
		return (ft_print_x_l(ap, f));
	else if (format == 'X')
		return (ft_print_lx_l(ap, f));
	else if (format == 'd' || format == 'i')
		return (ft_print_d_l(ap));
	else if (format == 'o')
		return (ft_print_o_l(ap, f));
	else if (format == 'O')
		return (ft_print_lo_l(ap, f));
	else if (format == 'u')
		return (ft_print_u_l(ap));
	else if (format == 'U')
		return (ft_print_lu_l(ap, f));
	else if (format == 'p')
		return (ft_print_p_point(ap, 0, f, *f));
	else if (format == 'c')
		return (ft_print_lc(ap, 0));
	else if (format == 's')
		return (ft_print_ls(ap));
	else if (format == 'D')
		return (ft_print_ld_l(ap));
	else if (format == '#')
		return (check_diese_l_after(ap, f));
	return (0);
}
