/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 15:20:50 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/08 15:20:54 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_str(const char *f, va_list ap, int i, int ret)
{
	int		p;

	p = 0;
	while (f[i])
	{
		if (f[i] != '%')
			ret = ret + ft_putchar(f[i++]);
		else if (f[i] == '%' && f[i + 1] != '-')
		{
			p = (f[i + 1] == cc1(f[i + 1])) ? check_flag(ap, f, i + 1) : 0;
			i = check_conv(f, ++i);
			(p < 0) ? i++ : (ret += p + check_type(ap, f[i], f, i));
			i++;
		}
		else if (f[i] == '%' && f[i + 1] == '-')
		{
			p = (f[i + 1] == cc1(f[i + 1])) ? check_flag(ap, f, i + 1) : 0;
			i = check_conv_moins(f, ++i);
			ret += p + check_type(ap, f[i], f, i);
			(f[i] == cc2(f[i])) ? i++ : (i = check_conv(f, ++i) + 1);
		}
	}
	return (ret);
}

int			ft_printf(const char *format, ...)
{
	va_list		ap;
	int			ret;

	ret = 0;
	va_start(ap, format);
	ret = var_printf(format, ap);
	va_end(ap);
	return (ret);
}
