/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_lc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:28:49 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/22 12:41:17 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_lc_3(unsigned int v, unsigned char octet)
{
	unsigned char	o4;
	unsigned char	o3;
	unsigned char	o2;
	unsigned char	o1;
	unsigned int	mask3;

	o4 = (v << 26) >> 26;
	o3 = ((v >> 6) << 26) >> 26;
	o2 = ((v >> 12) << 26) >> 26;
	o1 = ((v >> 18) << 29) >> 29;
	mask3 = 4034953344;
	octet = (mask3 >> 24) | o1;
	write(1, &octet, 1);
	octet = ((mask3 << 8) >> 24) | o2;
	write(1, &octet, 1);
	octet = ((mask3 << 16) >> 24) | o3;
	write(1, &octet, 1);
	octet = ((mask3 << 24) >> 24) | o4;
	write(1, &octet, 1);
	return (4);
}

int		ft_lc_2(unsigned int v, unsigned char octet)
{
	unsigned char	o3;
	unsigned char	o2;
	unsigned char	o1;
	unsigned int	mask2;

	o3 = (v << 26) >> 26;
	o2 = ((v >> 6) << 26) >> 26;
	o1 = ((v >> 12) << 28) >> 28;
	mask2 = 14712960;
	octet = (mask2 >> 16) | o1;
	write(1, &octet, 1);
	octet = ((mask2 << 16) >> 24) | o2;
	write(1, &octet, 1);
	octet = ((mask2 << 24) >> 24) | o3;
	write(1, &octet, 1);
	return (3);
}

int		ft_lc_1(unsigned int v, unsigned char octet)
{
	unsigned char	o2;
	unsigned char	o1;
	unsigned int	mask1;

	o2 = (v << 26) >> 26;
	o1 = ((v >> 6) << 27) >> 27;
	mask1 = 49280;
	octet = (mask1 >> 8) | o1;
	write(1, &octet, 1);
	octet = ((mask1 << 24) >> 24) | o2;
	write(1, &octet, 1);
	return (2);
}

int		ft_lc_0(unsigned int v, unsigned char octet)
{
	unsigned int	mask0;

	mask0 = 0;
	write(1, &octet, 1);
	return (1);
}

int		ft_print_lc(va_list ap, wchar_t value)
{
	unsigned char	octet;
	int				size;
	int				res;
	unsigned int	v;

	if (value == 0)
		value = va_arg(ap, wchar_t);
	size = ft_strlen(ft_itoa_base(value, 2));
	v = value;
	octet = value;
	res = 0;
	if (size <= 7)
		res = ft_lc_0(v, octet);
	else if (size <= 11)
		res = ft_lc_1(v, octet);
	else if (size <= 16)
		res = ft_lc_2(v, octet);
	else if (size <= 20)
		res = ft_lc_3(v, octet);
	else
		return (-1);
	return (res);
}
