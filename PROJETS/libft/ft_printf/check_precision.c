/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_precision.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 11:17:18 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/14 11:17:19 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		put_space_d_prec(va_list ap, const char *f, int i, int res)
{
	int		t[4];

	t[0] = (f[i] >= '0' && f[i] <= '9') ? ft_atoi(&f[i]) : 0;
	t[1] = len_ap(ap, f, i);
	t[1] = check_spe_case(ap, f, i, t[1]);
	t[2] = 0;
	t[3] = val_ap(ap) * -1;
	i = check_conv(f, i);
	if ((f[i] == 'X' || f[i] == 'x') && val_ap(ap) != 0)
	{
		res -= 2;
		t[2] = 2;
	}
	else if ((f[i] == 'O' || f[i] == 'o') && val_ap(ap) != 0)
	{
		res -= 2;
		t[2] = 1;
	}
	res = (t[0] == 0) ? res - t[2] : res;
	res = (res > t[1]) ? res - t[0] : res;
	t[0] = (t[0] > t[1]) ? t[0] - t[1] : 0;
	if ((f[i] == 'd' || f[i] == 'i') && val_ap(ap) < 0)
		return (p_s(res - 1, ap) + ft_putchar('-') + p_z(t[0] + 1, ap) \
				+ t[2] + ft_printf("%d", t[3]));
		return (p_s(res, ap) + put_spe_case(f, i) + p_z(t[0], ap) + t[2]);
}

int		ret_prec(const char *f, int i, va_list ap)
{
	int		t[3];

	t[0] = check_conv(f, i);
	t[1] = check_conv(f, i);
	t[2] = (f[i] >= '0' && f[i] <= '9') ? (ft_atoi(&f[i])) : 0;
	while (f[i--] != '%')
		if (f[i] == '#' && (f[t[0]] == 'o' || f[t[0]] == 'O'))
			return (ft_putchar('0'));
		else if (f[i] == '0' || f[i - 1] == '#')
			if (f[t[1]] == 'd' || f[t[1]] == 'i')
				return (p_z(t[2] - 1, 0));
	if (f[t[0]] == 'x' || f[t[0]] == 'X' || f[t[0]] == 'o' \
		|| f[t[0]] == 'O' || f[t[0]] == 'u' || f[t[0]] == 'U')
		return (p_z(t[2] - len_ap(ap, f, t[0]), ap));
	else if (f[t[0]] == 'p')
		return (ft_print_p_point(ap, t[2], f, i));
	else if ((f[t[0]] == 'd' || f[t[0]] == 'i') && (val_ap(ap) < 0) && t[2] > 0)
		return (p_z(t[2] - len_ap(ap, f, i), ap));
	else if ((f[t[0]] == 'd' || f[t[0]] == 'i') && (val_ap(ap) < 0) && t[2] < 1)
		return (ft_print_d(ap));
	else if (f[t[0]] == 'S' || f[t[0]] == 's')
		return (ft_print_ls_point(ap, t[2]));
	else if (f[t[0]] == 'c')
		return (ft_print_c(ap));
	return (0);
}

int		check_prec_neg(va_list ap, const char *f, int i, int res)
{
	char	*s;
	int		len;

	len = len_ap(ap, f, i);
	s = NULL;
	if (val_ap(ap) < 0)
	{
		len = check_spe_case(ap, f, i, len);
		if (f[i] == 'X' || f[i] == 'x' || f[i] == 'o' || f[i] == 'O')
		{
			if (res < len)
				return (0);
			else
				return (p_z(res - len, ap));
		}
		res -= len - 1;
		if (res < 0)
			return (ft_printf("-%d", (val_ap(ap) * -1)));
		while (res-- > 0)
			s = ft_properjoin(s, "0");
		return (ft_printf("-%s%d", s, (val_ap(ap) * -1)));
	}
	return (0);
}

int		check_prec_conv(va_list ap, const char *format, int i, int res)
{
	int		len;
	int		tmp;

	len = len_ap(ap, format, i);
	tmp = i;
	len = check_spe_case(ap, format, i, len);
	if (val_ap(ap) < 0)
		return (check_prec_neg(ap, format, i, res));
	while (format[tmp--] != '%')
	{
		if (format[tmp] == '-' && format[tmp - 1] == '%')
			return (p_z(res - len, ap) + \
			check_type(ap, format[i], &format[i - 1], i));
	}
	if (res > len)
		return (p_z(res - len, ap));
	return (0);
}

int		check_precision(va_list ap, const char *format, int i)
{
	int		res;

	if (format[i] == '*')
		return (ft_wildcard(ap, format, i));
	if (format[i] >= '0' && format[i] <= '9')
		res = ft_atoi(&format[i]);
	else
		return (ret_prec(format, i, ap));
	if ((long)val_ap(ap) == 0)
		return (ret_prec(format, i, ap));
	i = check_conv(format, i);
	if (format[i] == 's')
		return (ft_print_s_point(ap, res));
	else if (format[i] == 'S')
		return (ft_print_ls_point(ap, res));
	else if (format[i] == 'c')
		return (ft_print_c_point(ap, res));
	else if (format[i] == 'C')
		return (0);
	else if (format[i] == 'p')
		return (ft_print_p_point(ap, res, format, i));
	else if (format[i] == '%')
		return (ft_putchar('%'));
	return (check_prec_conv(ap, format, i, res));
}
