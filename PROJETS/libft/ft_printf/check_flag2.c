/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_flag2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 18:03:54 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/12 18:03:55 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_flag2_2(va_list ap, const char *format, int i)
{
	if (format[i] == '*')
		return (ft_wildcard(ap, format, i));
	return (0);
}

int		check_flag2(va_list ap, const char *format, int i)
{
	if (format[i] == 'z')
	{
		i = check_conv(format, i);
		return (check_z(ap, format[i], format[i]));
	}
	else if (format[i] == 'j')
	{
		i = check_conv(format, i);
		return (check_j(ap, format[i], format[i]));
	}
	else if (format[i] == '#' && format[i + 1] != '0')
	{
		while (format[i] == '#')
			i++;
		if (format[i] == '-')
			return (check_diese_moins(ap, format, i + 1));
		else if (format[i] == '+')
			return (check_plus_prec(ap, format, i + 1));
		return (check_diese(ap, &format[i]));
	}
	else if (format[i] >= '0' && format[i] <= '9')
		return (put_space(ap, format, i));
	return (check_flag2_2(ap, format, i));
}
