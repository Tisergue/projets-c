/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_o.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:39:50 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:39:52 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_o_h(va_list ap)
{
	short int		o;
	char			*s;

	o = va_arg(ap, int);
	s = ft_itoa_base((unsigned short int)o, 8);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_o_l(va_list ap, const char *f)
{
	unsigned long int		o;
	char					*s;

	o = va_arg(ap, unsigned long int);
	s = ft_itoa_base_ulint(o, 8);
	ft_putstr(s);
	while (*f--)
	{
		if (*f == '#')
			return (ft_strlen(s) + 1);
	}
	return (ft_strlen(s));
}

int		ft_print_o_j(va_list ap)
{
	unsigned long long		o;
	char					*s;

	o = va_arg(ap, unsigned long long);
	s = ft_itoa_base_long_long(o, 8);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_o_z(va_list ap)
{
	unsigned long		o;
	char				*s;

	o = va_arg(ap, unsigned long);
	s = ft_itoa_base_long(o, 8);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_o(va_list ap)
{
	unsigned int		o;
	char				*s;

	o = va_arg(ap, unsigned int);
	s = ft_itoa_base_printf_maj(o, 8);
	ft_putstr(s);
	return (ft_strlen(s));
}
