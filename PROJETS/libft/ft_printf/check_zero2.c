/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_zero2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 12:13:03 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/03 12:13:04 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_diese_zero(va_list ap, const char *f, int i)
{
	char	*s;
	int		t[4];

	s = NULL;
	t[1] = (f[i] >= '0' && f[i] <= '9') ? ft_atoi(&f[i]) : 0;
	t[0] = len_ap(ap, f, i);
	t[0] = check_spe_case(ap, f, i, t[0]);
	t[3] = return_res2_moins(f, i, ap);
	i = check_conv(f, i);
	t[1] -= t[0];
	t[3] -= t[0];
	if (t[3] > t[1])
		t[1] = 0;
	if (f[i] == 'd' && (t[2] = (long)val_ap(ap)) < 0 && t[1] > 0)
		return (ft_printf("-") + p_z(t[1], ap) + ft_printf("%d", (t[2] * -1)));
	else if (f[i] == 'x' || f[i] == 'X' || f[i] == 'o' || f[i] == 'O')
		return (spe_case_diese_z(ap, f, i, t[1]));
	if (len_ap(ap, f, i) < 1)
		return (len_ap(ap, f, i));
	return (p_s(t[1], ap) + p_z(t[3], ap));
}

int		p_z_p_conv_ret(va_list ap, const char *f, int i, int res)
{
	int		res2;

	if (f[i] == 'o' || f[i] == 'O')
		res2 = ft_strlen(ft_itoa_base((long)val_ap(ap), 8));
	else if (f[i] == 'x' || f[i] == 'X')
		res2 = ft_strlen(ft_itoa_base((long)val_ap(ap), 16));
	return (res2);
}

int		p_z_p_ret(va_list ap, const char *f, int i, int res)
{
	int		res2;

	if (f[i] >= '0' && f[i] <= '9')
		res2 = ft_atoi(&f[i]);
	else if (!(f[i] >= '0' && f[i] <= '9'))
		res2 = 0;
	else if ((long)val_ap(ap) == 0)
		return (p_z(res, ap));
	else
		return (p_z(res - (long)len_ap(ap, f, i), ap));
	return (res2);
}

int		put_zero_point2(va_list ap, const char *f, int i, int res)
{
	int		t[4];
	char	*check;

	t[0] = p_z_p_ret(ap, f, i, res);
	t[1] = 0;
	t[2] = check_conv(f, i);
	t[3] = (long)len_ap(ap, f, i);
	check = "";
	(f[t[2]] == 'c' && val_ap(ap) == 0) ? (res -= 1) : 0;
	(f[t[2]] == 'p') ? (t[1] = -1) : 0;
	if (f[t[2]] == 'o' || f[t[2]] == 'O' \
		|| f[t[2]] == 'x' || f[t[2]] == 'X')
		t[3] = p_z_p_conv_ret(ap, f, t[2], res);
	if (res > t[0])
	{
		(t[0] < t[3] && (long)val_ap(ap) < 0) ? (res -= t[3] - t[0] - 1) : 0;
		if (val_ap_char(ap) == check)
			return (p_z(res, ap) + check_precision(ap, f, i));
		((long)val_ap(ap) < 0) ? (t[1] = -1) : 0;
		(t[0] < t[3] && f[i + 1] != 's' && (long)val_ap(ap) > 0) ?
		(res -= t[3] - t[1]) : (res -= t[0] - t[1]);
		if (res > 0)
			return (p_z(res, ap) + check_precision(ap, f, i));
	}
	return (check_precision(ap, f, i));
}

int		put_zero_point(va_list ap, const char *f, int i, int res)
{
	int		t[2];

	t[0] = p_z_p_ret(ap, f, i, res);
	t[1] = check_conv(f, i);
	if (f[t[1]] == 's' || f[t[1]] == 'S' || f[t[1]] == 'c' || f[t[1]] == 'C')
	{
		if (val_ap(ap) == 0 || val_ap_char(ap) == NULL)
		{
			if (f[t[1]] == 'C')
				return (p_z(res - 1, ap));
			if (f[t[1]] == 'c')
				return (p_z(res - 1, ap) + ft_print_c(ap));
		}
		else if (res < t[0] && t[0] > ft_strlen(val_ap_char(ap)))
			return (p_z(res - ft_strlen(val_ap_char(ap)), ap) \
				+ check_precision(ap, f, i));
		else if (res > t[0] && t[0] > ft_strlen(val_ap_char(ap)))
			return (p_z(res - ft_strlen(val_ap_char(ap)), ap) \
				+ check_precision(ap, f, i));
		else if (t[0] == 0)
			return (p_z(res, ap));
	}
	if (f[t[1]] == '%')
		return (p_z(res - 1, ap) + ft_printf("%%"));
	return (put_zero_point2(ap, f, i, res));
}
