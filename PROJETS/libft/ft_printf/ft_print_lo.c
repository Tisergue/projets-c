/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_lo.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:30:56 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:30:57 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_lo_l(va_list ap, const char *f)
{
	signed char				lo;
	char					*s;

	lo = va_arg(ap, int);
	s = ft_itoa_base((unsigned short int)lo, 8);
	ft_putstr(s);
	while (*f--)
	{
		if (*f == '#')
			return (ft_strlen(s) + 1);
	}
	return (ft_strlen(s));
}

int		ft_print_lo_j(va_list ap)
{
	unsigned long long		lo;
	char					*s;

	lo = va_arg(ap, unsigned long long);
	s = ft_itoa_base_long_long(lo, 8);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_lo(va_list ap)
{
	long long	lo;
	char		*s;

	lo = va_arg(ap, long long);
	s = ft_itoa_base_long(lo, 8);
	ft_putstr(s);
	return (ft_strlen(s));
}
