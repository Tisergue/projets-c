/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_zero.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 12:22:01 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/13 12:22:02 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_zero_moins2(va_list ap, const char *f, int i, int res)
{
	int		t[5];

	t[0] = check_mpp_ret(f, i, ap);
	t[1] = len_ap(ap, f, i);
	t[1] = check_spe_case(ap, f, i, t[1]);
	t[3] = 0;
	t[4] = return_res2_moins(f, i, ap);
	i = check_conv(f, i);
	if (t[4] == 0 && t[0] == 1 && val_ap(ap) >= 0)
		(t[4] == 0) ? (res += 1) : 0;
	if (t[4] > 0 && t[0] == 1 && val_ap(ap) >= 0)
		(t[4] > 0) ? (res += 1) : 0;
	t[2] = t[1];
	if (t[4] > t[1])
	{
		t[3] = t[4] - t[1];
		t[1] = t[3];
	}
	res = (t[2] > t[4]) ? (res - t[2]) : (res - t[4]);
	if ((f[i] == 'd' || f[i] == 'i') && val_ap(ap) < 0)
		return (ft_putchar('-') + p_z(t[3] + 1, ap) + \
		ft_printf("%d", val_ap(ap) * -1) + p_s(res - t[0] - 1, ap) + t[0]);
		return (p_z(t[3], ap) + check_type(ap, f[i], &f[i - 1], i) \
		+ p_s(res - t[0], ap) + t[0]);
}

int		check_zero_moins(va_list ap, const char *f, int i, int res)
{
	int		t[2];

	t[0] = len_ap(ap, f, i);
	t[0] = check_spe_case(ap, f, i, t[0]);
	res = check_mpp(f, i, res, ap);
	t[1] = return_res2_moins(f, i, ap);
	if (res < t[1])
	{
		i = check_conv(f, i);
		if ((f[i] == 'd' || f[i] == 'i'))
			return (check2(ap, res, t[1], t[0]));
		return (check1(ap, res, t[1], t[0]) + \
			check_type(ap, f[i], &f[i - 1], i));
	}
	if (res < 1)
		return (0);
	return (check_zero_moins2(ap, f, i, res));
}

int		check_zero3(va_list ap, const char *f, int i, int res)
{
	char	*s;
	int		len;

	s = NULL;
	len = len_ap(ap, f, i);
	if (res == 0)
		return (0);
	if ((long)val_ap(ap) < 0 && (f[i] == 'd' || f[i] == 'i'))
	{
		res -= len;
		while (res-- > 0)
			s = ft_properjoin(s, "0");
		ft_printf("-%s%d", s, ((long)val_ap(ap) * -1));
		return (ft_strlen(s) + len);
	}
	if (f[i] == 's')
		len = ft_strlen(val_ap_char(ap));
	if (f[i] == 'c' && val_ap(ap) > 0)
		len -= 1;
	len = check_spe_case(ap, f, i, len);
	return (p_z(res - len, ap));
}

int		check_zero2(va_list ap, const char *f, int i, int res)
{
	char	*s;

	s = NULL;
	if (f[i] == '%')
	{
		res -= 1;
		while (res-- > 0)
			s = ft_properjoin(s, "0");
		ft_printf("%s%%", s);
		return (ft_strlen(s) + 1);
	}
	else if (f[i] == 'S')
		return (p_z(res - ft_len_ls(ap), ap));
	else if (f[i] == 'p' && (long)val_ap(ap) == 0)
	{
		res -= 2;
		while (res-- > 0)
			s = ft_properjoin(s, "0");
		ft_printf("0x%s", s);
		return (ft_strlen(s) + 2);
	}
	return (check_zero3(ap, f, i, res));
}

int		check_zero(va_list ap, const char *f, int i)
{
	int		res;
	int		tmp;
	int		tmp2;

	res = (f[i] >= '0' && f[i] <= '9') ? (ft_atoi(&f[i])) : 0;
	if (f[i] == '+')
		return (check_zero_plus(ap, f, i + 1));
	tmp = check_conv(f, i);
	while (f[tmp--] != '%')
		if (f[tmp] == '.')
		{
			tmp2 = check_conv(f, i);
			if (f[tmp2] == 's' || f[tmp2] == 'c' || f[tmp2] == '%' \
				|| f[tmp2] == 'S' || f[tmp2] == 'C')
				return (put_zero_point(ap, f, tmp + 1, res));
			return (put_space_point(ap, f, tmp + 1, res));
		}
	if (f[i - 1] == ' ')
	{
		if ((long)val_ap(ap) >= 0)
			return (ft_putchar(' ') + p_z(res - len_ap(ap, f, i) - 1, ap));
		else
			return (p_z(res - len_ap(ap, f, i) - 1, ap));
	}
	return (check_zero2(ap, f, i = check_conv(f, i), res));
}
