/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_char.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 20:55:02 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/29 20:55:04 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		p_s(int res, va_list ap)
{
	char	*s;

	s = NULL;
	if (res <= 0)
		return (0);
	while (res-- > 0)
		s = ft_properjoin(s, " ");
	ft_putstr(s);
	return (ft_strlen(s));
}

int		p_z(int res, va_list ap)
{
	char	*s;

	s = NULL;
	if (res <= 0)
		return (0);
	while (res-- > 0)
		s = ft_properjoin(s, "0");
	ft_putstr(s);
	return (ft_strlen(s));
}
