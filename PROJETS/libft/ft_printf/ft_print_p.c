/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_p.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:40:46 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:40:48 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_p_size(va_list ap)
{
	va_list						apc;
	unsigned long long			p;
	char						*s;

	va_copy(apc, ap);
	p = va_arg(apc, unsigned long long);
	s = ft_itoa_base_long_long(p, 16);
	va_end(apc);
	return (ft_strlen(s) + 2);
}

int		ft_check_p(va_list ap, const char *f, int i, int res)
{
	int		len;

	if (val_ap(ap) == 0)
	{
		check_type(ap, f[i], &f[i - 1], i);
		return (p_s(res - 3, ap) + 3);
	}
	len = check_type(ap, f[i], &f[i - 1], i);
	return (p_s(res - len, ap) + len);
}

int		ft_print_p_point(va_list ap, int res, const char *f, int i)
{
	char	*s;
	int		len;

	s = NULL;
	ft_putstr("0x");
	if (val_ap(ap) == 0)
	{
		if (res > 0)
			return (p_z(res, ap) + 2);
		return (2);
	}
	if (res > ilen_ap(ap))
	{
		len = ft_print_p_size(ap) - 2;
		res -= len;
		return (p_z(res, ap) + ft_print_p(ap));
	}
	else
		return (ft_print_p(ap));
	return (0);
}

int		ft_print_p(va_list ap)
{
	unsigned long long			p;
	char						*s;

	p = va_arg(ap, unsigned long long);
	s = ft_itoa_base_long_long(p, 16);
	ft_putstr(s);
	return (ft_strlen(s) + 2);
}
