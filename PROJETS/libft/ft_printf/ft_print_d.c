/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_d.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:27:57 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:27:58 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_d_h(va_list ap)
{
	short int		d;
	char			*s;

	d = va_arg(ap, int);
	s = ft_itoa(d);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_d_l(va_list ap)
{
	long int				d;
	char					*s;

	d = va_arg(ap, long int);
	s = ft_itoa_base_l(d, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_d_j(va_list ap)
{
	long long				d;
	char					*s;

	d = va_arg(ap, long long);
	s = ft_itoa_base_ll(d, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_d_z(va_list ap)
{
	long long int		d;
	char				*s;

	d = va_arg(ap, long long int);
	s = ft_itoa_base_llint(d, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_d(va_list ap)
{
	int		d;
	char	*s;

	d = va_arg(ap, int);
	s = ft_itoa(d);
	ft_putstr(s);
	return (ft_strlen(s));
}
