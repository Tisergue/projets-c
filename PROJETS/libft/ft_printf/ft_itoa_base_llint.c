/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_llint.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 12:41:44 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/08 12:41:45 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*ft_return(long long int n, int base, char *str, int i)
{
	int						p;
	unsigned long long int	f;

	p = 0;
	(n == 0) ? str[i++] = n + '0' : 0;
	if (n < 0)
	{
		n *= -1;
		p = 1;
	}
	f = n;
	while (f > 0)
	{
		if (base > 10 && f % base > 9)
			str[i++] = f % base - 10 + 'a';
		else
			str[i++] = f % base + '0';
		f /= base;
	}
	(p == 1) ? str[i++] = '-' : 0;
	str[i] = '\0';
	ft_strrev(str);
	return (str);
}

static int	ft_char_nb(long long int n, int base)
{
	int		i;

	i = 1;
	if (n == 0)
		return (1);
	else
	{
		while (n > 0)
		{
			n /= base;
			i++;
		}
	}
	return (i);
}

char		*ft_itoa_base_llint(long long int n, int base)
{
	char					*str;
	int						i;

	i = 0;
	str = ft_strnew(ft_char_nb(n, base));
	if (!str || (base < 2 || base > 36))
		return (NULL);
	return (ft_return(n, base, str, i));
}
