/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_plus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 15:35:33 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/20 15:35:33 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			check_plus_prec3(va_list ap, int res, int res2, int ret)
{
	int		check;
	int		ret_s;
	int		neg;

	check = 0;
	if (val_ap(ap) >= 0)
	{
		res -= ret + 1;
		check = 1;
	}
	if (val_ap(ap) < 0)
	{
		neg = val_ap(ap) * -1;
		res -= ret + 1;
		res2 -= ilen_ap(ap) - 1;
		ret_s = p_s(res, ap);
		return (ret_s + ft_putchar('-') + p_z(res2, ap) + ft_printf("%d", neg));
	}
	ret_s = p_s(res, ap);
	if (check == 1)
		ft_putchar('+');
	res2 -= ilen_ap(ap);
	return (ret_s + p_z(res2, ap) + check);
}

int			check_plus_prec2(va_list ap, const char *f, int i, int res)
{
	char	*s[2];
	int		t[5];

	t[3] = len_ap(ap, f, i);
	t[3] = check_spe_case(ap, f, i, t[3]);
	s[0] = NULL;
	s[1] = NULL;
	i = check_conv(f, i);
	t[1] = return_res2_plus(f, i);
	if (t[1] < t[3])
		t[1] = t[3];
	if (res > t[1])
	{
		t[2] = t[1];
		if (f[i] == 'd' || f[i] == 'i')
			return (check_plus_prec3(ap, res, t[2], t[1]));
		(val_ap(ap) >= 0) ? (res -= t[1]) : (res -= t[1]);
		t[2] -= t[3];
		return (p_s(res, ap) + p_z(t[2], ap));
	}
	return (0);
}

int			check_plus_prec(va_list ap, const char *f, int i)
{
	int		t[4];

	t[0] = (f[i] >= '0' && f[i] <= '9') ? ft_atoi(&f[i]) : 0;
	t[1] = len_ap(ap, f, i);
	t[1] = check_spe_case(ap, f, i, t[1]);
	t[2] = i;
	t[2] = check_conv(f, i);
	if (f[t[2]] == 's' || f[t[2]] == 'S' || f[t[2]] == 'c' || f[t[2]] == 'C')
		return (put_space_point(ap, f, t[2], t[0]));
	t[3] = return_res2_plus(f, t[2]);
	if (t[3] < t[1])
		t[3] = t[1];
	if (t[0] > t[3])
		return (check_plus_prec2(ap, f, i, t[0]));
	return (return_res2_big(f, i, t[3], ap));
}

int			put_space_plus(va_list ap, const char *f, int i, int res)
{
	char	*s;

	s = NULL;
	if ((long)val_ap(ap) >= 0)
	{
		res -= (long)len_ap(ap, f, i) + 1;
		while (res-- > 0)
			s = ft_properjoin(s, " ");
		ft_printf("%s+", s);
		return (ft_strlen(s) + 1);
	}
	else
	{
		res -= (long)len_ap(ap, f, i);
		while (res-- > 0)
			s = ft_properjoin(s, " ");
		ft_printf("%s", s);
	}
	return (ft_strlen(s));
}

int			check_plus(va_list ap, const char *f, int i)
{
	int		res;
	int		tmp;
	int		len;

	tmp = i;
	len = len_ap(ap, f, i);
	if (f[i] == '0')
		return (check_zero_plus(ap, f, i + 1));
	res = (f[tmp] >= '0' && f[tmp] <= '9') ? ft_atoi(&f[tmp]) : 0;
	if (f[i] == '-')
	{
		if (val_ap(ap) >= 0)
			return (ft_putchar('+') + p_s(res - len - 1, ap));
		return (check_moins(ap, f, i + 1));
	}
	i = check_conv(f, i);
	len = check_spe_case(ap, f, i, len);
	if (f[i] == 's')
		return (p_s(res - ft_strlen(val_ap_char(ap)), ap) + ft_print_s(ap));
	if (f[i] == 'd' || f[i] == 'i')
		if ((long)val_ap(ap) >= 0)
			return (p_s(res - len - 1, ap) + ft_putchar('+'));
	return (p_s(res - len, ap));
}
