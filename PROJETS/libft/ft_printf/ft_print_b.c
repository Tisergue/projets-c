/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_b.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:24:44 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:24:55 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_b(va_list ap)
{
	int		b;
	char	*s;

	b = va_arg(ap, int);
	s = ft_itoa_base(b, 2);
	ft_putstr(s);
	return (ft_strlen(s));
}
