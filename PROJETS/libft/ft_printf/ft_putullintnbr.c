/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putullintnbr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 12:45:27 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/08 12:45:28 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

long long int	ft_putullintnbr(long long int nb)
{
	if (nb >= 10)
	{
		ft_putullintnbr(nb / 10);
		ft_putullintnbr(nb % 10);
	}
	else
		ft_putchar(nb + '0');
	return (nb);
}
