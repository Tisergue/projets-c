/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_u.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:41:51 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:41:52 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_u_h(va_list ap)
{
	short int		u;
	char			*s;

	u = va_arg(ap, int);
	s = ft_itoa_base((unsigned short int)u, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_u_l(va_list ap)
{
	long int				u;
	char					*s;

	u = va_arg(ap, long int);
	s = ft_itoa_base_long(u, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_u_j(va_list ap)
{
	unsigned long long		u;
	char					*s;

	u = va_arg(ap, unsigned long long);
	s = ft_itoa_base_long_long(u, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_u_z(va_list ap)
{
	long long int		u;
	char				*s;

	u = va_arg(ap, long long int);
	s = ft_itoa_base_long_long(u, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_u(va_list ap)
{
	unsigned int	u;
	char			*s;

	u = va_arg(ap, unsigned int);
	s = ft_itoa_base_long(u, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}
