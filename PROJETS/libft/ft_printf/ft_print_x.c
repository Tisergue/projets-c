/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_x.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:42:30 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:42:31 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_x_h(va_list ap)
{
	unsigned short int		x;
	char					*s;

	x = va_arg(ap, unsigned int);
	s = ft_itoa_base_printf_min((unsigned short int)x, 16);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_x_l(va_list ap, const char *f)
{
	long int				x;
	char					*s;

	x = va_arg(ap, long int);
	s = ft_itoa_base_long(x, 16);
	ft_putstr(s);
	while (*f--)
	{
		if (*f == '#')
			return (ft_strlen(s) + 2);
	}
	return (ft_strlen(s));
}

int		ft_print_x_j(va_list ap)
{
	unsigned long long		x;
	char					*s;

	x = va_arg(ap, unsigned long long);
	s = ft_itoa_base_long_long(x, 16);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_x_z(va_list ap)
{
	unsigned long		x;
	char				*s;

	x = va_arg(ap, unsigned long);
	s = ft_itoa_base_long(x, 16);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_x(va_list ap)
{
	int		x;
	char	*s;

	x = va_arg(ap, int);
	s = ft_itoa_base_printf_min(x, 16);
	ft_putstr(s);
	return (ft_strlen(s));
}
