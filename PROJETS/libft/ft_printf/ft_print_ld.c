/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_ld.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:30:10 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/12 14:30:12 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_ld_l(va_list ap)
{
	signed char				ld;
	char					*s;

	ld = va_arg(ap, int);
	s = ft_itoa_base((unsigned short int)ld, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_ld_h(va_list ap)
{
	unsigned short int	ld;
	char				*s;

	ld = va_arg(ap, unsigned int);
	s = ft_itoa_base((unsigned short int)ld, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}

int		ft_print_ld(va_list ap)
{
	long			ld;
	char			*s;

	ld = va_arg(ap, long);
	s = ft_itoa_base_l(ld, 10);
	ft_putstr(s);
	return (ft_strlen(s));
}
