/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   special_case.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 15:07:55 by tisergue          #+#    #+#             */
/*   Updated: 2016/03/14 15:07:56 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		put_spe_case(const char *f, int i)
{
	(f[i] == 'X') ? ft_putstr("0X") : 0;
	(f[i] == 'x') ? ft_putstr("0x") : 0;
	(f[i] == 'O' || f[i] == 'o') ? ft_putstr("0") : 0;
	return (0);
}

int		spe_case_diese_z(va_list ap, const char *f, int i, int res)
{
	int		d;

	d = 0;
	i = check_conv(f, i);
	if (f[i] == 'x' || f[i] == 'X')
	{
		if (val_ap(ap) == 0)
			return (ft_putchar('0'));
		(f[i] == 'x') ? (ft_putstr("0x")) : (ft_putstr("0X"));
		d = 2;
		res -= 2;
	}
	else if (f[i] == 'o' || f[i] == 'O')
	{
		if (val_ap(ap) == 0)
			return (ft_putchar('0'));
		(f[i] == 'o' && val_ap(ap) != 0) ? (ft_putstr("0")) : (ft_putstr("0"));
		d = 1;
		res -= 1;
	}
	if (len_ap(ap, f, i) < 1)
		return (len_ap(ap, f, i));
	return (p_z(res, ap) + d);
}

int		check_spe_case(va_list ap, const char *f, int i, int len)
{
	i = check_conv(f, i);
	if (f[i] == 'X' || f[i] == 'x')
		len = (f[i] == 'X' || f[i] == 'x') ? \
	ft_strlen(ft_itoa_base_printf_maj(val_ap(ap), 16)) : 0;
	if (f[i] == 'O' || f[i] == 'o')
		len = (f[i] == 'O' || f[i] == 'o') ? \
	ft_strlen(ft_itoa_base_printf_maj(val_ap(ap), 8)) : 0;
	if (f[i] == 'D')
		len = (f[i] == 'D') ? ft_strlen(ft_itoa_base_long(val_ap(ap), 10)) : 0;
	return (len);
}
