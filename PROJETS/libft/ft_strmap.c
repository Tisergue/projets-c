/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:05:45 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/01 12:22:35 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	int		j;
	char	*n;

	i = 0;
	if (s != NULL)
	{
		j = ft_strlen((char *)s);
		n = (char *)malloc(sizeof(char) * (j + 1));
		while (s[i])
		{
			n[i] = f(s[i]);
			i++;
		}
		n[i] = '\0';
		return (n);
	}
	return (NULL);
}
