/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 18:18:40 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/24 09:52:17 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	char	*d;
	char	*s;

	if (n == 0 || dest == src)
		return (dest);
	d = (char *)dest;
	s = (char *)src;
	while (--n)
		*d++ = *s++;
	*d = *s;
	return (dest);
}
