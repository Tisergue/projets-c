/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_properjoin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/28 15:07:55 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/28 15:08:20 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_properjoin(char *s1, char *s2)
{
	char	*ret;

	if (s1 == NULL && s2 == NULL)
		return (NULL);
	else if (s1 == NULL)
	{
		ret = ft_strdup(s2);
		return (ret);
	}
	else if (s2 == NULL)
	{
		ret = ft_strdup(s1);
		ft_strdel(&s1);
		return (ret);
	}
	else if (s1 != NULL && s2 != NULL)
	{
		ret = ft_strjoin(s1, s2);
		ft_strdel(&s1);
		return (ret);
	}
	return (NULL);
}
