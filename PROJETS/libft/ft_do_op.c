/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_do_op.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <tisergue@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/31 09:20:19 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/26 06:15:17 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_do_more_op(int nb1, char *op, int nb2, int i)
{
	int		res;

	if (op[i] == '*')
	{
		res = nb1 * nb2;
		ft_putnbr(res);
		i++;
		return (res);
	}
	if (op[i] == '/')
	{
		res = nb1 / nb2;
		ft_putnbr(res);
		i++;
		return (res);
	}
	if (op[i] == '%')
	{
		res = nb1 % nb2;
		ft_putnbr(res);
		i++;
		return (res);
	}
	return (0);
}

int				ft_do_op(int nb1, char *op, int nb2)
{
	int		res;
	int		i;

	i = 0;
	if (op[i] == '+')
	{
		res = nb1 + nb2;
		ft_putnbr(res);
		i++;
		return (res);
	}
	if (op[i] == '-')
	{
		res = nb1 - nb2;
		ft_putnbr(res);
		i++;
		return (res);
	}
	return (ft_do_more_op(nb1, op, nb2, i));
}
