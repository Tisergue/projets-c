/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:09:06 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/01 15:40:21 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	void		ft_banane(unsigned int nbp)
{
	if (nbp >= 10 && nbp < 4294967295)
	{
		ft_putnbr(nbp / 10);
		ft_putnbr(nbp % 10);
	}
	else
		ft_putchar(nbp + '0');
}

void				ft_putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb *= -1;
	}
	ft_banane(nb);
}
