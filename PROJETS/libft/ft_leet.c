/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_leet.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 15:22:35 by tisergue          #+#    #+#             */
/*   Updated: 2016/02/10 15:23:34 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		alpha_3(char *str, int i)
{
	if (str[i] == 'w' || str[i] == 'W')
		ft_putstr("vv ");
	if (str[i] == 'x' || str[i] == 'X')
		ft_putstr("× ");
	if (str[i] == 'y' || str[i] == 'Y')
		ft_putstr("φ ");
	if (str[i] == 'z' || str[i] == 'Z')
		ft_putstr(">_ ");
}

static void		alpha_2(char *str, int i)
{
	if (str[i] == 'l' || str[i] == 'L')
		ft_putstr("1 ");
	if (str[i] == 'm' || str[i] == 'M')
		ft_putstr("|\\/| ");
	if (str[i] == 'n' || str[i] == 'N')
		ft_putstr("/\\/ ");
	if (str[i] == 'o' || str[i] == 'O')
		ft_putstr("0 ");
	if (str[i] == 'p' || str[i] == 'P')
		ft_putstr("P ");
	if (str[i] == 'q' || str[i] == 'Q')
		ft_putstr("Q ");
	if (str[i] == 'r' || str[i] == 'R')
		ft_putstr("r ");
	if (str[i] == 's' || str[i] == 'S')
		ft_putstr("5 ");
	if (str[i] == 't' || str[i] == 'T')
		ft_putstr("¯|¯ ");
	if (str[i] == 'u' || str[i] == 'V')
		ft_putstr("µ ");
	if (str[i] == 'v' || str[i] == 'V')
		ft_putstr("|/ ");
}

static void		alpha_1(char *str, int i)
{
	if (str[i] == 'a' || str[i] == 'A')
		ft_putstr("4 ");
	if (str[i] == 'b' || str[i] == 'B')
		ft_putstr("8 ");
	if (str[i] == 'c' || str[i] == 'C')
		ft_putstr("< ");
	if (str[i] == 'd' || str[i] == 'D')
		ft_putstr("|) ");
	if (str[i] == 'e' || str[i] == 'E')
		ft_putstr("3 ");
	if (str[i] == 'f' || str[i] == 'F')
		ft_putstr("F ");
	if (str[i] == 'g' || str[i] == 'G')
		ft_putstr("6 ");
	if (str[i] == 'h' || str[i] == 'H')
		ft_putstr("H ");
	if (str[i] == 'i' || str[i] == 'I')
		ft_putstr("1 ");
	if (str[i] == 'j' || str[i] == 'J')
		ft_putstr("J ");
	if (str[i] == 'k' || str[i] == 'K')
		ft_putstr("K ");
}

void			ft_leet(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (str[i] == ' ')
			ft_putstr("  ");
		if ((str[i] >= 'a' && str[i] <= 'z')
			|| (str[i] >= 'A' && str[i] <= 'Z'))
		{
			if ((str[i] >= 'a' && str[i] <= 'k')
				|| (str[i] >= 'A' && str[i] <= 'K'))
				alpha_1(str, i);
			if ((str[i] >= 'l' && str[i] <= 'v')
				|| (str[i] >= 'L' && str[i] <= 'V'))
				alpha_2(str, i);
			if ((str[i] >= 'w' && str[i] <= 'z')
				|| (str[i] >= 'W' && str[i] <= 'Z'))
				alpha_3(str, i);
		}
		else
			write(1, &str[i], 1);
		i++;
	}
}
