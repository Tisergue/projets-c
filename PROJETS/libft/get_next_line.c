/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/05 18:42:57 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/18 17:02:49 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int				endl(char **line, char *chr, char *stock)
{
	*chr = '\0';
	*line = ft_strdup(stock);
	ft_memmove(stock, chr + 1, ft_strlen(chr + 1) + 1);
	return (1);
}

int						get_next_line(int const fd, char **line)
{
	static char		*stock[256];
	char			buf[BUFF_SIZE + 1];
	char			*chr;
	int				ret;

	if (line == NULL || fd < 0 || fd > 256)
		return (-1);
	ret = 1;
	while (ret > 0)
	{
		if (stock[fd] != NULL && (chr = ft_strchr(stock[fd], '\n')) != NULL)
			return (endl(line, chr, stock[fd]));
		ret = read(fd, buf, BUFF_SIZE);
		if (ret == -1)
			return (-1);
		buf[ret] = '\0';
		stock[fd] = ft_properjoin(stock[fd], buf);
	}
	if (ft_strlen(stock[fd]) > 0)
	{
		*line = stock[fd];
		stock[fd] = 0;
		return (1);
	}
	return (0);
}
