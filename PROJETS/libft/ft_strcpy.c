/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 18:22:57 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/24 14:34:27 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dest, const char *src)
{
	size_t	ptr;

	ptr = 0;
	while (src[ptr])
	{
		dest[ptr] = src[ptr];
		ptr++;
	}
	dest[ptr] = '\0';
	return (dest);
}
