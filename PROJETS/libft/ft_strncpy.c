/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 18:23:22 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/24 14:40:26 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	size_t	ptr;

	ptr = 0;
	while (ptr < n && src[ptr])
	{
		dest[ptr] = src[ptr];
		ptr++;
	}
	while (ptr < n)
	{
		dest[ptr] = '\0';
		ptr++;
	}
	return (dest);
}
