/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 18:20:13 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/24 09:55:37 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned char	*string;

	if (dest == '\0' || src == '\0')
		return (NULL);
	string = (unsigned char*)malloc(sizeof(char *) * n);
	ft_memcpy(string, src, n);
	ft_memcpy(dest, string, n);
	free(string);
	return (dest);
}
