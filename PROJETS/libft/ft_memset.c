/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 09:26:40 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/24 16:53:59 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *s, int c, size_t n)
{
	unsigned char	*ptr;

	if (n == 0)
		return (s);
	ptr = (unsigned char*)s;
	while (n--)
	{
		*ptr = (unsigned char)c;
		if (n)
			ptr++;
	}
	return (s);
}
