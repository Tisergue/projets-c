/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <tisergue@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/31 09:50:55 by tisergue          #+#    #+#             */
/*   Updated: 2015/07/31 13:13:38 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_max(int *tab, unsigned int len)
{
	unsigned int	i;
	int				k;

	i = 0;
	k = 0;
	while (i < len)
	{
		if (tab[i] > k)
			k = tab[i];
		i++;
	}
	return (k);
}
