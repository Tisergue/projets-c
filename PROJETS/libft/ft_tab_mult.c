/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tab_mult.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <tisergue@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/31 19:07:38 by tisergue          #+#    #+#             */
/*   Updated: 2015/07/31 20:24:44 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_tab_mult(int m)
{
	int i;
	int r;

	i = 1;
	while (i < 10)
	{
		r = i * m;
		ft_putnbr(i);
		ft_putstr(" x ");
		ft_putnbr(m);
		ft_putstr(" = ");
		ft_putnbr(r);
		ft_putchar('\n');
		i++;
	}
	return (r);
}
