/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:08:23 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/27 17:08:28 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char				*rev(char *s)
{
	char	stock;
	char	*str;

	str = s + ft_strlen(s) - 1;
	while (s < str)
	{
		stock = *s;
		*s++ = *str;
		*str-- = stock;
	}
	return (s);
}

static size_t			ft_number_len(int n)
{
	size_t	i;

	i = 1;
	while (n > 0)
	{
		n /= 10;
		i++;
	}
	return (i);
}

char					*ft_itoa(int n)
{
	char			*s;
	unsigned int	nb;
	int				index;

	index = 0;
	nb = ft_abs(n);
	s = ft_strnew(ft_number_len(nb));
	if (!s)
		return (NULL);
	if (nb == 0)
		s[index++] = nb + '0';
	while (nb > 0)
	{
		s[index++] = nb % 10 + '0';
		nb /= 10;
	}
	if (n < 0)
		s[index++] = '-';
	s[index] = '\0';
	rev(s);
	return (s);
}
