/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 14:19:33 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/04 14:19:34 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*list;
	t_list	*nxt;

	list = *alst;
	while (list)
	{
		nxt = list->next;
		del(list->content, list->content_size);
		free(list);
		list = nxt;
	}
	*alst = NULL;
}
