/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 11:24:04 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/26 11:24:58 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*locc;

	locc = NULL;
	while (*s)
	{
		if (*s == (char)c)
			locc = (char *)s;
		s++;
	}
	if (*s == (char)c)
		return ((char *)s);
	else
		return (locc);
}
