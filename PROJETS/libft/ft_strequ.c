/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:06:20 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/01 12:19:19 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strequ(char const *s1, char const *s2)
{
	if (s2 == NULL)
		return (-1);
	if (s1 == NULL)
		return (-1);
	if (ft_strcmp(s1, s2) == 0)
		return (1);
	else
		return (0);
}
