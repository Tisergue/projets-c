/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:08:03 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/27 17:08:10 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_banana_count(char const *s, char c)
{
	size_t	i;
	size_t	banane;

	i = 0;
	banane = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		if (s[i] != c && s[i])
		{
			banane++;
			while (s[i] != c && s[i])
				i++;
		}
	}
	return (banane);
}

static size_t	ft_banana_len(size_t i, const char *s, char c)
{
	size_t	n;

	n = 0;
	if (s == NULL)
		return (0);
	while (s[i] != c && s[i])
	{
		n++;
		i++;
	}
	return (n);
}

char			**ft_strsplit(char const *s, char c)
{
	size_t	i;
	size_t	j;
	char	**tab;

	tab = NULL;
	i = 0;
	j = 0;
	if (s)
		tab = (char **)malloc(sizeof(char *) * (ft_banana_count(s, c) + 1));
	if (tab)
	{
		while (j < ft_banana_count(s, c))
		{
			while (s[i] == c)
				i++;
			if (s[i] != c && s[i])
			{
				tab[j] = ft_strsub(s, i, ft_banana_len(i, s, c));
				i = i + ft_strlen(tab[j]);
				j++;
			}
		}
		tab[j] = 0;
	}
	return (tab);
}
