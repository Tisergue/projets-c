/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 14:31:32 by tisergue          #+#    #+#             */
/*   Updated: 2016/01/09 14:32:49 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_char_nb(unsigned int n, int base)
{
	int		i;

	i = 1;
	if (n == 0)
		return (1);
	else
	{
		while (n > 0)
		{
			n /= base;
			i++;
		}
	}
	return (i);
}

char		*ft_itoa_base(int n, int base)
{
	char			*str;
	int				i;
	unsigned int	nb;

	i = 0;
	nb = ft_abs(n);
	str = ft_strnew(ft_char_nb(nb, base));
	if (!str || (base < 2 || base > 36))
		return (NULL);
	if (nb == 0)
		str[i++] = nb + '0';
	while (nb > 0)
	{
		if (base > 10 && nb % base > 9)
			str[i++] = nb % base - 10 + 'A';
		else
			str[i++] = nb % base + '0';
		nb /= base;
	}
	if (n < 0)
		str[i++] = '-';
	str[i] = '\0';
	ft_strrev(str);
	return (str);
}
