/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 15:43:33 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/14 16:07:48 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_pow(int nb, int pow)
{
	int	res;

	res = 1;
	if (pow <= 0)
		return (0);
	while (pow != 0)
	{
		res *= nb;
		pow--;
	}
	return (res);
}
