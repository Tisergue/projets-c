/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:05:58 by tisergue          #+#    #+#             */
/*   Updated: 2015/12/01 12:22:53 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	i;
	int				j;
	char			*n;

	i = 0;
	n = NULL;
	if (s != NULL)
	{
		j = ft_strlen((char *)s);
		n = (char *)malloc(sizeof(char) * (j + 1));
		n[j] = '\0';
		while (s[i])
		{
			n[i] = f(i, s[i]);
			i++;
		}
		return (n);
	}
	return (NULL);
}
