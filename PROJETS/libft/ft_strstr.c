/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tisergue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 11:25:34 by tisergue          #+#    #+#             */
/*   Updated: 2015/11/26 11:25:49 by tisergue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *mdf, const char *a)
{
	int	i;
	int	j;
	int	k;

	i = 0;
	k = 0;
	if (!*a)
		return ((char *)mdf);
	while (mdf[i])
	{
		j = i;
		while (mdf[j] == a[k])
		{
			j++;
			k++;
			if (a[k] == '\0')
				return ((char *)(&mdf[i]));
		}
		k = 0;
		i++;
	}
	return (NULL);
}
