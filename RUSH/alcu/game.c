/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mseinic <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 14:11:20 by mseinic           #+#    #+#             */
/*   Updated: 2015/12/21 23:00:26 by mseinic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aluc.h"

void				message(int k)
{
	if (k == 0)
		ft_putstr("\x1B[32m----------------Enter a number : \x1B[00m");
	if (k == 1)
		ft_putstr("\x1B[31mTry again (hint - it's 1, 2 or 3 ) : \x1B[00m");
}

void				ft_winner(int gamer)
{
	if (gamer == 1)
		ft_putstr("\n\x1B[32m=========YOU WIN=========\x1B[00m\n\n");
	else
		ft_putstr("\n\x1B[32m=========YOU LOSE=========\x1B[00m\n\n");
}

static	void		cpu_choice(int n, int *tab)
{
	if (n == 1)
		ft_putstr("\x1B[33m----------------CPU choose 1\x1B[00m\n");
	if (n == 2)
		ft_putstr("\x1B[33m----------------CPU choose 2\x1B[00m\n");
	if (n == 3)
		ft_putstr("\x1B[33m----------------CPU choose 3\x1B[00m\n");
	ft_affich(tab);
	if (*tab != 0 || *(tab + 2) != -1)
		ft_putstr("\n\x1B[34m///////////////////Your turn\x1B[00m\n");
}

static	void		verif(int *tab, int i)
{
	if (tab[i] - 1 > 0 && (tab[i] - 1) % 4 == 1)
	{
		tab[i] -= 1;
		cpu_choice(1, tab);
	}
	else if (tab[i] - 2 > 0 && (tab[i] - 2) % 4 == 1)
	{
		tab[i] -= 2;
		cpu_choice(2, tab);
	}
	else if (tab[i] - 3 > 0 && (tab[i] - 3) % 4 == 1)
	{
		tab[i] -= 3;
		cpu_choice(3, tab);
	}
	else if (tab[i] != 0 && tab[i] - 1 >= 0)
	{
		tab[i]--;
		cpu_choice(1, tab);
	}
}

int					*game(int *tab)
{
	int i;

	i = 0;
	while (tab[i] == 0)
		i++;
	ft_putstr("\n----------------After your turn\n");
	ft_affich(tab);
	verif(tab, i);
	return (tab);
}
