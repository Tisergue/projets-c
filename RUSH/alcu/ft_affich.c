/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_affich.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mseinic <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 18:24:49 by mseinic           #+#    #+#             */
/*   Updated: 2015/12/21 22:57:43 by mseinic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aluc.h"

void	aff_matches(int *i)
{
	int		j;

	j = 0;
	if (*i < 10)
		ft_putstr("     ");
	else if (*i > 9 && *i < 100)
		ft_putstr("    ");
	else if (*i > 99 && *i < 1000)
		ft_putstr("   ");
	else
		ft_putstr("  ");
	while (j < *i)
	{
		ft_putstr("\x1B[36m|\x1B[00m");
		j++;
	}
}

void	ft_affich(int *tab)
{
	int j;

	j = 0;
	ft_putstr("\n");
	while (tab[j] != -1)
	{
		if (tab[j] != 0)
		{
			ft_putnbr(tab[j]);
			aff_matches(&tab[j]);
			ft_putstr("\n");
		}
		j++;
	}
}
