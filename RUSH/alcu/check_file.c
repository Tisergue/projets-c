/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mseinic <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 11:31:15 by mseinic           #+#    #+#             */
/*   Updated: 2015/12/21 22:58:52 by mseinic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aluc.h"

void	*create_tab(int i, int j, char *buff)
{
	int	*tab;

	if ((tab = (int *)malloc(i)) == NULL)
		return (NULL);
	i = 0;
	j = 0;
	tab[i] = ft_atoi(buff);
	if ((tab[i] < 1 || tab[i] > 10000))
		return (NULL);
	i++;
	while (buff[j] != '\0')
	{
		if (buff[j] == '\n' && buff[j + 1] != '\0')
		{
			j++;
			tab[i] = ft_atoi(buff + j);
			if ((tab[i] < 1 || tab[i] > 10000))
				return (NULL);
			i++;
		}
		j++;
	}
	tab[i] = -1;
	return (tab);
}

int		*check_file(int fd)
{
	char	buff[BUFF_SIZE];
	int		i;
	int		j;
	int		*tab;

	j = -1;
	i = 0;
	ft_bzero(buff, BUFF_SIZE);
	read(fd, buff, BUFF_SIZE);
	while (buff[++j] != '\0')
		if (buff[j] == '\n')
			i++;
	tab = create_tab(i, j, buff);
	return (tab);
}
