/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dispfiller.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mseinic <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 19:36:40 by mseinic           #+#    #+#             */
/*   Updated: 2015/12/21 22:58:34 by mseinic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aluc.h"

static	char	*ft_str(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\n' && str[i + 1] == '\n')
			str[i + 1] = '\0';
		i++;
	}
	return (str);
}

static	int		count(char *str)
{
	int n;
	int i;

	i = -1;
	n = 0;
	while (str[++i] != '\0')
		if (str[i] == '\n')
			n++;
	return (n);
}

int				*dispfiller(void)
{
	char	buff[BUFF_SIZE];
	int		j;
	int		*tab;
	char	str[BUFF_SIZE];

	ft_bzero(buff, BUFF_SIZE);
	j = 0;
	while (j == 0)
	{
		read(0, buff, BUFF_SIZE + 1);
		if (buff[0] == '\n')
			j = 1;
		ft_strcat(str, buff);
		ft_bzero(buff, BUFF_SIZE);
	}
	j = count(str);
	ft_str(str);
	tab = create_tab(j, 0, str);
	return (tab);
}
