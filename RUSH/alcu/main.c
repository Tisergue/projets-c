/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mseinic <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 11:11:59 by mseinic           #+#    #+#             */
/*   Updated: 2015/12/21 23:07:47 by mseinic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aluc.h"

static void		gamer1(int *tab, int *i, int fd, int *gamer)
{
	if (tab[*i] == 0)
		*i = *i + 1;
	if (tab[*i] - fd < 0 && *gamer == 0)
	{
		*gamer = 1;
		ft_putstr("\x1B[31m----------------not enought matches\x1B[00m\n");
		ft_putnbr(tab[*i]);
	}
	if (tab[*i] - fd >= 0)
	{
		*gamer = 0;
		tab[*i] -= fd;
	}
	if (tab[*i] == 0 && tab[*i + 1] != -1)
		*i = *i + 1;
}

static	void	gamer2(int *tab, int *i, int k, int *gamer)
{
	if (*gamer == 0 && k == 0 && tab[*i] != 0)
	{
		*gamer = 1;
		game(tab);
	}
}

static int		ft_alum(int *tab, char buff[BUFF_SIZE], int fd)
{
	int			t[3];
	int			i;

	t[2] = 0;
	i = 0;
	t[0] = 1;
	while (t[0])
	{
		t[1] = 0;
		ft_affich(tab);
		message(t[2]);
		read(0, buff, BUFF_SIZE);
		if (buff[0] > '0' && buff[0] < '4' && buff[1] == '\n')
		{
			fd = ft_atoi(buff);
			gamer1(tab, &i, fd, &t[1]);
			t[2] = 0;
		}
		else
			t[2] = 1;
		gamer2(tab, &i, t[2], &t[1]);
		if (tab[i] == 0 && tab[i + 1] == -1)
			t[0] = 0;
	}
	return (t[1]);
}

int				main(int ac, char **av)
{
	int		fd;
	char	buff[BUFF_SIZE];
	int		*tab;
	int		winner;

	fd = open(av[1], O_RDONLY);
	if (ac == 2)
	{
		if (fd < 0)
			return (ft_error());
		tab = check_file(fd);
		close(fd);
		if (tab == NULL)
			return (ft_error());
		winner = ft_alum(tab, buff, fd);
		ft_winner(winner);
	}
	else if (ac == 1)
	{
		tab = dispfiller();
		if (tab == NULL)
			return (ft_error());
		winner = ft_alum(tab, buff, fd);
		ft_winner(winner);
	}
}
