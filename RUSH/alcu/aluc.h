/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aluc.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mseinic <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 11:45:20 by mseinic           #+#    #+#             */
/*   Updated: 2015/12/21 21:55:54 by mseinic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ALUC_H
# define ALUC_H

# include "libft.h"

# include <fcntl.h>

# define BUFF_SIZE 2000000

void	*create_tab(int i, int j, char *buff);
void	aff_matches(int *i);
void	ft_winner(int gamer);
void	message(int k);
int		ft_error();
void	ft_affich(int *tab);
int		*dispfiller(void);
int		*check_file(int fd);
int		*game(int *tab);

#endif
